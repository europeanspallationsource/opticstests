/**
 * Asyn driver for the Thorlabs MFF.
 *
 * @author Hinko Kocevar
 * @date October 2015
 *
 */

#ifndef TLMFF101_H
#define TLMFF101_H

#include "asynPortDriver.h"
#include "ftd2xx.h"

#define TLMFF_BUFFER_SIZE		64

#define TlMessageString			"TL_MESSAGE"
#define TlMnfcString			"TL_MNFC"
#define TlProdString			"TL_PROD"
#define TlSerialString			"TL_SERIAL"
#define TlVIDPIDString			"TL_VIDPID"
#define TlFlipString			"TL_FLIP"
#define TlBlinkString			"TL_BLINK"

#define MFF101_VERTICAL			0x01
#define MFF101_HORIZONTAL		0x02

/**
 * Driver class for Thorlabs MFF. This inherits from AsynPortDriver class in asyn.
 *
 */
class TlMFF : public asynPortDriver {

public:
	TlMFF(const char *portName, int usbVID, int usbPID,
			const char *serialNumber, int priority, int stackSize);
	virtual ~TlMFF();

	/* These are the methods that we use */
	virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
	virtual void report(FILE *fp, int details);

	// Should be private, but are called from C so must be public
	void deviceTask(void);

private:
	void dumpBuffer(unsigned char *buffer, int elements);
	asynStatus getPosition();
	asynStatus doFlip();
	asynStatus doBlinkLED();

protected:
    int TlMessage;
    #define FIRST_COMMAND TlMessage
	int TlMnfc;
	int TlProd;
	int TlSerial;
	int TlVIDPID;
	int TlFlip;
	int TlBlink;
    #define LAST_COMMAND TlBlink

private:
	void addFTDevice(void);
	void removeFTDevice(void);
	unsigned int checkStatus(const char *fnc, FT_STATUS returnStatus);

	epicsEventId mDeviceEvent;
	unsigned int mFinish;

	// FT instrument handle
	FT_HANDLE mInstr;
	char mManufacturerName[TLMFF_BUFFER_SIZE];
	char mDeviceName[TLMFF_BUFFER_SIZE];
	char mSerialNumber[TLMFF_BUFFER_SIZE];
	char mWantSerialNumber[TLMFF_BUFFER_SIZE];
	epicsUInt16 mUsbVID;
	epicsUInt16 mUsbPID;
	int mPosition;
};

#define NUM_TL_DET_PARAMS (&LAST_COMMAND - &FIRST_COMMAND + 1)

#endif //TLMFF101_H

