Hinko Kocevar <hinko.kocevar@esss.se>
07 Oct 2015


Introduction
------------

This repository holds the software used in optics tests performed in the optics
lab and on site (Saclay, SNS). The purpose is to prepare IOC and OPI for
various camera and spectrometer control.

Currently supporting:

 - Andor Newton and Andor Luca (USB)
 - Thorlabs CCS 100 and CCS 200 spectrometers (USB)
 - Thorlabs PM100USB power meter (USB)
 - Allied Vision Tech Prosilica GX 1050 camera (Ethernet)
 - Allied Vision Tech Prosilica MANTA G-235B camera (Ethernet)
 - Basler acA2000 50gmNIR camera (Ethernet)
 - Thorlabs FW102 motorized filter wheel (USB)
 - Thorlabs MFF101 filter flipper (USB)
 
See devices folder for more information about the devices.

area detector 1.9.1
-------------------

Several modifications were introduced to the code, database and OPI to support
features that are not part of stock package.
See areadetector-1.9.1/git.log or create a diff against clean areadetector-1.9.1
source to see the differences.
  
EPICS base and support modules
-------------------------------

EPICS base and other support modules are not part of this code base (they need
to be present on the target system).

$MY_TOP/epics									top EPICS folder
$MY_TOP/epics/base-3.14.12.5					top EPICS base folder
$MY_TOP/epics/base								link to base-3.14.12.5

$MY_TOP/epics/support							top EPICS support module folder
$MY_TOP/epics/support/asyn4-26					asyn driver
$MY_TOP/epics/support/autosave-5-6-1			autosave support
$MY_TOP/epics/support/busy-1-6-1				busy record
$MY_TOP/epics/support/calc-3-4-2				calc record
$MY_TOP/epics/support/sscan-2-10				sscan record

$MY_TOP/epics/extensions						top EPICS extensions folder
											(extract extensionsTop_20120904.tar here)
$MY_TOP/epics/extensions/src/StreamDevice-2-6	stream device support
$MY_TOP/epics/extensions/src/msi1-7			msi utility

See epics folder for RELEASE files and handy build and clean scripts to speed up
the process of getting EPICS base and support modules in place.

CSS and OPI
-----------

EPICS CSS is not part of this code base (it needs to be present on the target
system). Tested with CSS 3.2.16.

OPI files in areadetector-1.9.1/ADApp/op/opi folder are left as-is. OPI files
for this project were collected in the folder opi and adapted where needed.

See this page for CSS download https://ics-web.sns.ornl.gov/css/products.html.


aravis
------

For GigE cameras aravis and aravisGigE software packages need to be in used.
See http://controls.diamond.ac.uk/downloads/support/aravisGigE/.

Latest aravigGigE (0.3.5dls5) and aravis (0.3.7) packages were used.


GNU supporting software
-----------------------

For USB devices libusb-1.0 needs to be installed in the host computer.

For Ubuntu-like Linux OS APT repository provided libusb-1.0 can be used.

For MAC OS X the libusb-1.0 package can be obtained from rudix software
repository (or macports or any other GNU repository fro MAC OS).
See rudix section below.

See also http://www.libusb.org/ and
http://sourceforge.net/projects/libusb/files/libusb-1.0/libusb-1.0.19.

Latest libusb-1.0 (1.0.19) package was used.

For aravis glib2 is required. This can also be obtained from rudix (or macports
or any other GNU repository fro MAC OS). In turn glib2 will depend on libxml2,
freetype, etc. which can also be obtained from rudix.

Make sure that EPICS base (and support modules) see the GNU supporting software
(add include / library folders to compilation directives).

rudix
-----

Rudix can compile and package GNU software into MAC pkg files. Later these
pkg files can be installed on a target volume.

Note that if using 'open' command on MAC or double-click on the pkg file
MAC installer will complain about that it can not install on non / volume!

To install "package.pkg" onto volume <volume> use the following command:

$ cd rudix
$ sudo python Library/installer.py --target=/Volumes/<volume> Ports/<dir>/package.pkg

Package contents will appear in <volume>/usr/local subfolders.

Mandatory rudix provided packages used to compile this package are:
freetype-2.5.2-0.pkg
gettext-0.19.4-0.pkg
glib-2.44.1-0.pkg
intltool-0.50.2-0.pkg
libusb-1.0.19-1.pkg
libxml2-2.9.2-0.pkg
pkg-config-0.28-1.pkg
rudix-2015.6.1-0.pkg
zlib-1.2.8-0.pkg

Note that if pkg-config is used to deliver compiler and linker flags then
PKG_CONFIG needs to be set and 'prefix' found in the .pc files needs to be
overridden to reflect <volume>/usr/local subfolders. For example:

PKGCONFIG := pkg-config --print-errors --define-variable=prefix=<volume>/usr/local

See also http://rudix.org

USB devices
-----------

Make sure that proper udev rules are installed in orderto use the USB devices
See corresponding folders for *.rules files that need to be placed into
/etc/udev/rules.d. Applies to Linux only.


NOTES
-----

- Thorlabs PM100USB is supported via streamdevice
- For MAC compilation of base add the '-stdlib=libstdc++' to variables SHRLIB_LDFLAGS
  and LOADABLE_SHRLIB_LDFLAGS in the file
  $MY_TOP/epics/base-3.14.12.5/configure/os/CONFIG.darwinCommon.darwinCommon
- For MAC compilation against libusb-1.0 add the following to the
  $MY_TOP/epics/base-3.14.12.5/configure/os/CONFIG_SITE.darwinCommon.darwinCommon:

  #HK path to the GNU libs
  OP_SYS_INCLUDES += -I/usr/local/include
  OP_SYS_LDFLAGS +=  $(addprefix -L,$(dir $(firstword $(wildcard /usr/local/lib/*))))
  
  This will use libusb-1.0 provided by rudix for asynUSBTMC and tlccs.
- be sure to remove *.la files from installed rudix GNU libraries as they might interfere
  with the aravis linking process