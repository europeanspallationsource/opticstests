epicsEnvSet("PREFIX", "ALUCA:")
epicsEnvSet("PORT",   "ALUCA")
epicsEnvSet("CAM",    "cam1:")

# save things every thirty seconds
create_monitor_set("andorLuca_auto_settings.req", 30,"P=$(PREFIX)")
