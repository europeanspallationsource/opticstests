# Do not run this file directly!
#
# Include this file in the top level st.cmd if you want support for Thorlabs
# power meter PM100USB.

###############################################################################
# START PM100 power meter
# usbtmcConfigure(port, vendorNum, productNum, serialNumberStr, priority, flags)
usbtmcConfigure("PM100")
asynSetTraceIOMask("PM100",0,0xff)
#asynSetTraceMask("PM100",0,0xff)

# Load record instances
dbLoadRecords("$(TLPM)/db/tlPM100.template","P=PM100:,R=,PORT=PM100")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=PM100:,R=asyn,PORT=PM100,ADDR=0,OMAX=100,IMAX=100")

# END PM100 power meter
###############################################################################

