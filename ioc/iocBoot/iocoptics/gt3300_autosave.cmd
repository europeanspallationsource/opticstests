epicsEnvSet("PREFIX", "GT3300:")
epicsEnvSet("PORT",   "GT3300")
epicsEnvSet("CAM",    "cam1:")

# save things every thirty seconds
create_monitor_set("gt3300_auto_settings.req", 30,"P=$(PREFIX)")
