epicsEnvSet("PREFIX", "MANTA235:")
epicsEnvSet("PORT",   "MANTA235")
epicsEnvSet("CAM",    "cam1:")

# save things every thirty seconds
create_monitor_set("mantag235b_auto_settings.req", 30,"P=$(PREFIX)")
