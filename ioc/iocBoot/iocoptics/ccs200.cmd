# Do not run this file directly!
#
# Include this file in the top level st.cmd if you want support for Thorlabs
# spectrometer CCS200 with serial number M00310535.
# Resource string: USB::0x1313::0x8089::M00310535::RAW

###############################################################################
# START CCS200 spectrum analyzer
epicsEnvSet("RSCSTR", "USB::0x1313::0x8089::M00310535::RAW")
epicsEnvSet("PREFIX", "CCS200:")
epicsEnvSet("PORT",   "CCS200")
epicsEnvSet("CAM",    "cam1:")
epicsEnvSet("QSIZE",  "20")
epicsEnvSet("XSIZE",  "3648")
epicsEnvSet("YSIZE",  "1")
epicsEnvSet("NCHANS", "2048")
epicsEnvSet("FWPATH", "$(LIBTLCCS)/Loader")

# tlCCS100Config(const char *portName, int maxBuffers, size_t maxMemory, 
#                const char *firmwarePath, const char *resourceName, int priority, int stackSize)
tlCCS100Config("$(PORT)", 0, 0, "$(FWPATH)", "$(RSCSTR)", 0, 100000)

# Load record instances
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template","P=$(PREFIX),R=$(CAM),PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template","P=$(PREFIX),R=$(CAM),PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/tlCCS.template", "P=$(PREFIX),R=$(CAM),PORT=$(PORT),ADDR=0,TIMEOUT=1,NELEMENTS=3648")

# Create a standard arrays plugin
NDStdArraysConfigure("$(PORT)TRACE1", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Trace1:,PORT=$(PORT)TRACE1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=$(PREFIX),R=Trace1:,PORT=$(PORT)TRACE1,ADDR=0,TIMEOUT=1,TYPE=Float64,FTVL=DOUBLE,NELEMENTS=4000")

# Load all other plugins

# Create a TIFF file saving plugin
NDFileTIFFConfigure("$(PORT)TIFF1", $(QSIZE), 0, "$(PORT)", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Tiff1:,PORT=$(PORT)TIFF1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",      "P=$(PREFIX),R=Tiff1:,PORT=$(PORT)TIFF1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFileTIFF.template",  "P=$(PREFIX),R=Tiff1:,PORT=$(PORT)TIFF1,ADDR=0,TIMEOUT=1")

# Create a ASCII file saving plugin
NDFileAsciiConfigure("$(PORT)ASCII1", $(QSIZE), 0, "$(PORT)", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Ascii1:,PORT=$(PORT)ASCII1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",      "P=$(PREFIX),R=Ascii1:,PORT=$(PORT)ASCII1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFileAscii.template", "P=$(PREFIX),R=Ascii1:,PORT=$(PORT)ASCII1,ADDR=0,TIMEOUT=1")

# Create a ROI plugin
NDROIConfigure("$(PORT)ROI1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=ROI1:,  PORT=$(PORT)ROI1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROI.template",       "P=$(PREFIX),R=ROI1:,  PORT=$(PORT)ROI1,ADDR=0,TIMEOUT=1")

# Create a processing plugin
NDProcessConfigure("$(PORT)PROC1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Proc1:,  PORT=$(PORT)PROC1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDProcess.template",   "P=$(PREFIX),R=Proc1:,  PORT=$(PORT)PROC1,ADDR=0,TIMEOUT=1")

# Create a statistics plugin
NDStatsConfigure("$(PORT)STATS1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Stats1:,  PORT=$(PORT)STATS1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=ROI1,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStats.template",     "P=$(PREFIX),R=Stats1:,  PORT=$(PORT)STATS1,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS)")

# Create a source plugin
NDSourceConfigure("$(PORT)SOURCE1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Source1:,  PORT=$(PORT)SOURCE1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDSource.template",    "P=$(PREFIX),R=Source1:,  PORT=$(PORT)SOURCE1,ADDR=0,TIMEOUT=1,SOURCE=tlpm:test:MEAS:POW,NELEMENTS=100")

set_requestfile_path("./")
set_requestfile_path("$(AREA_DETECTOR)/ADApp/Db")
set_requestfile_path("$(CALC)/calcApp/Db")
set_requestfile_path("$(SSCAN)/sscanApp/Db")
set_requestfile_path("$(TLCCS)/Db")
set_savefile_path("./autosave")
set_pass0_restoreFile("ccs200_auto_settings.sav")
set_pass1_restoreFile("ccs200_auto_settings.sav")
save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("$(AUTOSAVE)/asApp/Db/save_restoreStatus.db", "P=$(PREFIX)")

#asynSetTraceMask("$(PORT)",0,3)
#asynSetTraceIOMask("$(PORT)",0,4)

# END CCS200 spectrum analyzer
###############################################################################
