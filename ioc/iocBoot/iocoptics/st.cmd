
< envPaths

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "50000000")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(TLPM)/db:$(TLFW)/db")
errlogInit(20000)

epicsEnvSet "EPICS_TS_MIN_WEST", '0'

# Loading libraries
# -----------------

dbLoadDatabase "$(TOP)/dbd/optics.dbd"
optics_registerRecordDeviceDriver(pdbbase)

# Device initialisation
# ---------------------

#< ccs100.cmd
#< ccs200.cmd
#< pm100usb.cmd
#< fw102usb.cmd
#< mff101usb.cmd
#< gx1050.cmd
#< mantag235b.cmd
#< aca2000.cmd
#< gt3300.cmd
< andorLuca.cmd

# Final ioc initialisation
# ------------------------

iocInit

#< ccs100_autosave.cmd
#< ccs200_autosave.cmd
#< pm100usb_autosave.cmd
#< fw102usb_autosave.cmd
#< mff101usb_autosave.cmd
#< gx1050_autosave.cmd
#< mantag235b_autosave.cmd
#< aca2000_autosave.cmd
#< gt3300_autosave.cmd
< andorLuca_autosave.cmd
