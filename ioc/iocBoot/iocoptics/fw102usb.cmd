# Do not run this file directly!
#
# Include this file in the top level st.cmd if you want support for Thorlabs
# motorized filter wheel FW102C.

#var streamDebug 1

###############################################################################
# START FW102 motorized filter wheel

epicsEnvSet("SERIAL_PORT", "/dev/ttyUSB0")

# drvAsynSerialPortConfigure(port, ttyName, priority, noAutoConnect, noProcessEosIn)
drvAsynSerialPortConfigure("FW102", "$(SERIAL_PORT)", 0, 0, 0)
asynSetOption("FW102", 0, "baud", "115200")
asynSetOption("FW102", 0, "bits", "8")
asynSetOption("FW102", 0, "parity", "none")
asynSetOption("FW102", 0, "stop", "1")
asynSetOption("FW102", 0, "clocal", "Y")
asynSetOption("FW102", 0, "crtscts", "N")

# handled by the Terminator = CR; in the proto file
#asynOctetSetInputEos( "FW102", 0, "\r")
#asynOctetSetOutputEos("FW102", 0, "\r")

asynSetTraceIOMask("FW102",0,0xff)
#asynSetTraceMask("FW102",0,0xff)

# Load record instances
dbLoadRecords("$(TLFW)/db/tlFW102.template","P=FW102:,R=,PORT=FW102")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=FW102:,R=asyn,PORT=FW102,ADDR=0,OMAX=100,IMAX=100")

# END FW102 motorized filter wheel
###############################################################################

