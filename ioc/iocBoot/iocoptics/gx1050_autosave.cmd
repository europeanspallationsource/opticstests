epicsEnvSet("PREFIX", "GX1050:")
epicsEnvSet("PORT",   "GX1050")
epicsEnvSet("CAM",    "cam1:")

# save things every thirty seconds
create_monitor_set("gx1050_auto_settings.req", 30,"P=$(PREFIX)")
