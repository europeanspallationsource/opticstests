# Do not run this file directly!
#
# Include this file in the top level st.cmd if you want support for Thorlabs
# filter flipper MFF101.

###############################################################################
# START MFF101 filter flipper
# tlMFF101Config(port, vendorNum, productNum, serialNumberStr, priority, flags)
tlMFF101Config("MFF101", 0x0403, 0xFAF0, "37861633")
asynSetTraceIOMask("MFF101",0,0xff)
#asynSetTraceMask("MFF101",0,0xff)

# Load record instances
dbLoadRecords("$(TLMFF)/db/tlMFF101.template","P=MFF101:,R=,PORT=MFF101,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=MFF101:,R=asyn,PORT=MFF101,ADDR=0,OMAX=100,IMAX=100")

# END MFF101 filter flipper
###############################################################################

