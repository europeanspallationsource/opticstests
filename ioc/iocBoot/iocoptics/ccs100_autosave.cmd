epicsEnvSet("PREFIX", "CCS100:")
epicsEnvSet("PORT",   "CCS100")
epicsEnvSet("CAM",    "cam1:")

# save things every thirty seconds
create_monitor_set("ccs_auto_settings.req", 30,"P=$(PREFIX),D=$(CAM)")
