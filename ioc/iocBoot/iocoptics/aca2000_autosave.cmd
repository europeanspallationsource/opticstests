epicsEnvSet("PREFIX", "ACA2000:")
epicsEnvSet("PORT",   "ACA2000")
epicsEnvSet("CAM",    "cam1:")

# save things every thirty seconds
create_monitor_set("aca2000_auto_settings.req", 30,"P=$(PREFIX)")
