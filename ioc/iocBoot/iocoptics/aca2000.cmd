# Do not run this file directly!
#
# Include this file in the top level st.cmd if you want support for Basler
# GigE camera acA2000 50gmNIR with serial number 21729661.
# Resource string: Basler-21729661

###############################################################################
# START ACA2000 GigE camera
epicsEnvSet("RSCSTR", "Basler-21729661")
epicsEnvSet("PREFIX", "ACA2000:")
epicsEnvSet("PORT",   "ACA2000")
epicsEnvSet("CAM",    "cam1:")
epicsEnvSet("QSIZE",  "20")
epicsEnvSet("XSIZE",  "2048")
epicsEnvSet("YSIZE",  "1080")
epicsEnvSet("NCHANS", "2048")
# NELEM = XSIZE * YSIZE
epicsEnvSet("NELEM",  "2097152")

# aravisCameraConfig(portName, cameraName, maxBuffers, maxMemory)
aravisCameraConfig("$(PORT)", "$(RSCSTR)", 50, -1)

# Load record instances
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",        "P=$(PREFIX),R=$(CAM),PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",        "P=$(PREFIX),R=$(CAM),PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("$(ARAVISGIGE)/db/aravisCamera.template",           "P=$(PREFIX),R=$(CAM),PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("$(ARAVISGIGE)/db/Basler_acA2000_50gmNIR.template", "P=$(PREFIX),R=$(CAM),PORT=$(PORT),ADDR=0,TIMEOUT=1")

# Load all other plugins

# Use SHORT FTVL in order not to see negative values in the ArrayData when using UInt8 data from camera
NDStdArraysConfigure("$(PORT)IMAGE1", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Image1:,PORT=$(PORT)IMAGE1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=$(PREFIX),R=Image1:,PORT=$(PORT)IMAGE1,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEM)")
NDStdArraysConfigure("$(PORT)IMAGE2", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Image2:,PORT=$(PORT)IMAGE2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=$(PREFIX),R=Image2:,PORT=$(PORT)IMAGE2,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEM)")
NDStdArraysConfigure("$(PORT)IMAGE3", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Image3:,PORT=$(PORT)IMAGE3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=$(PREFIX),R=Image3:,PORT=$(PORT)IMAGE3,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEM)")
NDStdArraysConfigure("$(PORT)IMAGE4", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Image4:,PORT=$(PORT)IMAGE4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=$(PREFIX),R=Image4:,PORT=$(PORT)IMAGE4,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEM)")
NDStdArraysConfigure("$(PORT)IMAGE5", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Image5:,PORT=$(PORT)IMAGE5,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=$(PREFIX),R=Image5:,PORT=$(PORT)IMAGE5,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEM)")

# Create 5 transform plugins
NDTransformConfigure("$(PORT)TRANS1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Trans1:,PORT=$(PORT)TRANS1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDTransform.template", "P=$(PREFIX),R=Trans1:,PORT=$(PORT)TRANS1,ADDR=0,TIMEOUT=1")
NDTransformConfigure("$(PORT)TRANS2", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Trans2:,PORT=$(PORT)TRANS2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDTransform.template", "P=$(PREFIX),R=Trans2:,PORT=$(PORT)TRANS2,ADDR=0,TIMEOUT=1")
NDTransformConfigure("$(PORT)TRANS3", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Trans3:,PORT=$(PORT)TRANS3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDTransform.template", "P=$(PREFIX),R=Trans3:,PORT=$(PORT)TRANS3,ADDR=0,TIMEOUT=1")
NDTransformConfigure("$(PORT)TRANS4", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Trans4:,PORT=$(PORT)TRANS4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDTransform.template", "P=$(PREFIX),R=Trans4:,PORT=$(PORT)TRANS4,ADDR=0,TIMEOUT=1")
NDTransformConfigure("$(PORT)TRANS5", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Trans5:,PORT=$(PORT)TRANS5,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDTransform.template", "P=$(PREFIX),R=Trans5:,PORT=$(PORT)TRANS5,ADDR=0,TIMEOUT=1")

# Create a TIFF file saving plugin
NDFileTIFFConfigure("$(PORT)TIFF1", $(QSIZE), 0, "$(PORT)", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Tiff1:,PORT=$(PORT)TIFF1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",      "P=$(PREFIX),R=Tiff1:,PORT=$(PORT)TIFF1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFileTIFF.template",  "P=$(PREFIX),R=Tiff1:,PORT=$(PORT)TIFF1,ADDR=0,TIMEOUT=1")
NDFileTIFFConfigure("$(PORT)TIFF2", $(QSIZE), 0, "$(PORT)", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Tiff2:,PORT=$(PORT)TIFF2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",      "P=$(PREFIX),R=Tiff2:,PORT=$(PORT)TIFF2,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFileTIFF.template",  "P=$(PREFIX),R=Tiff2:,PORT=$(PORT)TIFF2,ADDR=0,TIMEOUT=1")

# Create a ROI plugin
NDROIConfigure("$(PORT)ROI1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=ROI1:,  PORT=$(PORT)ROI1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROI.template",       "P=$(PREFIX),R=ROI1:,  PORT=$(PORT)ROI1,ADDR=0,TIMEOUT=1")
NDROIConfigure("$(PORT)ROI2", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=ROI2:,  PORT=$(PORT)ROI2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROI.template",       "P=$(PREFIX),R=ROI2:,  PORT=$(PORT)ROI2,ADDR=0,TIMEOUT=1")
NDROIConfigure("$(PORT)ROI3", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=ROI3:,  PORT=$(PORT)ROI3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROI.template",       "P=$(PREFIX),R=ROI3:,  PORT=$(PORT)ROI3,ADDR=0,TIMEOUT=1")

# Create a processing plugin
NDProcessConfigure("$(PORT)PROC1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Proc1:,  PORT=$(PORT)PROC1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDProcess.template",   "P=$(PREFIX),R=Proc1:,  PORT=$(PORT)PROC1,ADDR=0,TIMEOUT=1")
NDProcessConfigure("$(PORT)PROC2", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Proc2:,  PORT=$(PORT)PROC2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDProcess.template",   "P=$(PREFIX),R=Proc2:,  PORT=$(PORT)PROC2,ADDR=0,TIMEOUT=1")
NDProcessConfigure("$(PORT)PROC3", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Proc3:,  PORT=$(PORT)PROC3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDProcess.template",   "P=$(PREFIX),R=Proc3:,  PORT=$(PORT)PROC3,ADDR=0,TIMEOUT=1")

# Create a statistics plugin
NDStatsConfigure("$(PORT)STATS1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Stats1:,  PORT=$(PORT)STATS1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStats.template",     "P=$(PREFIX),R=Stats1:,  PORT=$(PORT)STATS1,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS)")
NDStatsConfigure("$(PORT)STATS2", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Stats2:,  PORT=$(PORT)STATS2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStats.template",     "P=$(PREFIX),R=Stats2:,  PORT=$(PORT)STATS2,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS)")
NDStatsConfigure("$(PORT)STATS3", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Stats3:,  PORT=$(PORT)STATS3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStats.template",     "P=$(PREFIX),R=Stats3:,  PORT=$(PORT)STATS3,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS)")
NDStatsConfigure("$(PORT)STATS4", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Stats4:,  PORT=$(PORT)STATS4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStats.template",     "P=$(PREFIX),R=Stats4:,  PORT=$(PORT)STATS4,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS)")
NDStatsConfigure("$(PORT)STATS5", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=Stats5:,  PORT=$(PORT)STATS5,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStats.template",     "P=$(PREFIX),R=Stats5:,  PORT=$(PORT)STATS5,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS)")

set_requestfile_path("./")
set_requestfile_path("$(AREA_DETECTOR)/ADApp/Db")
set_requestfile_path("$(ARAVISGIGE)/aravisGigEApp/Db")
set_requestfile_path("$(CALC)/calcApp/Db")
set_requestfile_path("$(SSCAN)/sscanApp/Db")
set_savefile_path("./autosave")
set_pass0_restoreFile("aca2000_auto_settings.sav")
set_pass1_restoreFile("aca2000_auto_settings.sav")
save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("$(AUTOSAVE)/asApp/Db/save_restoreStatus.db", "P=$(PREFIX)")

#asynSetTraceMask("$(PORT)",0,3)
#asynSetTraceIOMask("$(PORT)",0,4)

# END ACA2000 GigE camera
###############################################################################
