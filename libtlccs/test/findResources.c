//==============================================================================
//
// Title:      findResources.c
// Purpose:    Simple pure C program to list the attached
//             Thorlabs CCS - Compact Spectrometer.
//             Based on sample.c from Thorlabs.
//
// Created on: Jul-8-2015
// Author:     Hinko Kocevar (hinko.kocevar@esss.se)
// Copyright:  Thorlabs. All Rights Reserved.
//
//==============================================================================

//==============================================================================
// Include files
//===========================================================================
#include <stdio.h>      // stdio for file operations
#include <stdlib.h>
#include "../src/visa.h"       // we need visa (typically found in your VXIPNP\include directory)
#include "../src/tlccs.h"      // the device driver header

//==============================================================================
// Constants
//===========================================================================

#define FWPATH                "/lib/firmware/ccs"   // default firmware path

//===========================================================================
// Globals
//===========================================================================

//===========================================================================
// Prototypes
//===========================================================================

void error_exit(ViStatus err);

//==============================================================================
// Main
//==============================================================================
int main (int argc, char *argv[])
{
	ViStatus    err      = VI_SUCCESS;           // error variable
	ViSession   resMgr   = VI_NULL;              // resource manager
	ViUInt32    cnt      = 0;                    // counts found devices
	ViChar      rscStr[VI_FIND_BUFLEN];          // resource string
	ViChar*     fwPathPtr = FWPATH;
	ViFindList  list;

	// get path to the firmware files
	if (getenv("FWPATH") != NULL) {
		fwPathPtr = getenv("FWPATH");
	}

	printf("Thorlabs CCS instrument finder application\n");

	// Find resources
	printf("Scanning for CCS instruments ...\n");
	if((err = viOpenDefaultRM(&resMgr))) {
		error_exit(err);
	}
	if((err = viSetFirmwareFolder(resMgr, fwPathPtr))) {
		error_exit(err);
	}
	if((err = viFindRsrc(resMgr, TLCCS_FIND_PATTERN, &list, &cnt, rscStr))) {
		error_exit(err);
	}

	printf("Found %u instrument%s ...\n", cnt, (cnt>1) ? "s" : "");
	cnt = 1;
	if (cnt) {
		cnt = 1;
		do {
			// first resource is returned by the viFindRsrc()
			printf("Instrument [%d]: '%s'\n", cnt, rscStr);
			cnt++;
		} while ((err = viFindNext(list, rscStr) == VI_SUCCESS));
	}

	viClose(resMgr);

	// leave main
	return err;
}


/*---------------------------------------------------------------------------
  Error exit
---------------------------------------------------------------------------*/
void error_exit(ViStatus err)
{
	ViChar ebuf[TLCCS_ERR_DESCR_BUFFER_SIZE];

	// Print error
	tlccs_error_message (VI_NULL, err, ebuf);
	fprintf(stderr, "ERROR: %s\n", ebuf);

	exit (err);
}
