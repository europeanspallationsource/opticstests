#!/bin/bash

# three packets (setup, data, status) were merged into single message
# check that we did it ok - msg indexes should increment by three now

p=
for i in $(cat indexes)
do
	if [ -z "$p" ]
	then
		p="$i"
	else
		d=$(($i-$p))
		if [ "$d" -ne 3 ]
		then
			echo "ERROR at i = $i, p = $p"
			exit 1
		fi
#		echo $d
		p="$i"
	fi
	
done

echo "ALL FINE"
exit 0
