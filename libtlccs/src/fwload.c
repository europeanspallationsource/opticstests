/*
 * fwload.c
 *
 *  Created on: Jan 25, 2015
 *      Author: hinxx
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <libusb-1.0/libusb.h>

#include "common.h"

struct fwinfo {
	uint16_t vid;
	uint16_t pid;
	char file[256];
};

struct fwheader {
	char tag[4];					/* tag string: CSPT */
	uint32_t msgLen;				/* length of this message w/ header */
	uint32_t hdrLen;				/* length of header 32 (0x20) bytes */
	unsigned char res00000050[4];	/* ?? always 00 00 00 50 */
	unsigned char bRequest;			/* bRequest in USB msg, always A0 */
	unsigned char res70;			/* ?? always 70 */
	uint16_t wValue;				/* wValue in USB msg */
	uint16_t wIndex;				/* wIndex in USB msg, always 00 00 */
	unsigned char res6C65[2];		/* ?? always 6C 65 */
	unsigned char res0F000000[4];	/* ?? always 0F 00 00 00 */
	uint32_t wLength;				/* wLength in USB msg */
};

struct fwinfo fwinfos[5] = {
		{ 0x1313, 0x8080, "CCS100.spt" },
		{ 0x1313, 0x8082, "CCS125.spt" },
		{ 0x1313, 0x8084, "CCS150.spt" },
		{ 0x1313, 0x8086, "CCS175.spt" },
		{ 0x1313, 0x8088, "CCS200.spt" }
};

int fwload(uint16_t vid, uint16_t pid, char *fwPath) {
	int i;
	int r;
	unsigned char buf[1024];
	struct fwheader hdr;
	char filePath[1024] = {0};
	int fp = -1;
	libusb_device_handle *dev = NULL;

	if (strlen(fwPath) == 0) {
		ERR("CCS firmware dir not set!\n");
		return -1;
	}

	for (i = 0; i < 5; i++) {
		if (vid == fwinfos[i].vid && pid == fwinfos[i].pid) {
			sprintf(filePath, "%s/%s", fwPath, fwinfos[i].file);
			break;
		}
	}

	if (! *filePath) {
		ERR("CCS with VID %X and PID %X not found!\n", vid, pid);
		return -1;
	}
	DBG("Found CCS firmware %s for VID %X and PID %X\n", filePath, vid, pid);

	fp = open(filePath, O_RDONLY);
	if (fp < 0) {
		ERR("open() failed: %s\n", strerror(errno));
		return -1;
	}

	r = usbOpen(vid, pid, &dev);
	if (r) {
		ERR("usbOpen() failed\n");
		close(fp);
		return -1;
	}

	do {
		/* get header */
		r = read(fp, &hdr, sizeof(struct fwheader));
		if (r == 0) {
			DBG("end of file..\n");
			break;
		} else if (r < 0) {
			ERR("read() failed: %s\n", strerror(errno));
			close(fp);
			usbClose(dev);
			return -1;
		} else if (r != sizeof(struct fwheader)) {
			ERR("read() was short: %s\n", strerror(errno));
			close(fp);
			usbClose(dev);
			return -1;
		}
		DBG("TAG:         %.*s\n", 4, hdr.tag);
		DBG("wValue:      0x%X\n", hdr.wValue);
		DBG("wLength:     0x%X\n", hdr.wLength);
		r = read(fp, buf, hdr.wLength);
		if (r < 0) {
			ERR("read() failed: %s\n", strerror(errno));
			close(fp);
			usbClose(dev);
			return -1;
		} else if (r == 0) {
			DBG("end of file?? should not happen here!\n");
			close(fp);
			usbClose(dev);
			return -1;
		}

		DBG("data length: 0x%X (%d)\n", r, r);

		r = libusb_control_transfer(
				dev,						/* handle */
				0x40,		 				/* bmRequestType */
				hdr.bRequest,				/* bRequest */
				hdr.wValue,					/* wValue */
				hdr.wIndex,					/* wIndex */
				buf,						/* data buffer */
				hdr.wLength,				/* data size */
				1000						/* timeout */
				);
		if (r < 0) {
	    	ERR("libusb_control_transfer failed (%d): %s\n", r, libusb_strerror(r));
	    	close(fp);
	    	usbClose(dev);
	    	return -1;
		}

	} while (r > 0);
	close(fp);

	/* Last message sent to the USB device needs to be with proper wValue! */
	if (hdr.wValue == 0xE600) {
		DBG("Successfully uploaded firmware %s\n", filePath);
	} else {
		return -1;
	}

	r = usbClose(dev);
	if (r) {
		ERR("usbClose() failed\n");
		return -1;
	}
	dev = NULL;

	DBG("USB device ready in 5 seconds..\n");
	sleep(5);
	DBG("USB device ready!\n");


	/* At this point green LED should be light up on the device. */

	return 0;
}
