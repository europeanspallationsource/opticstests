/*
 * common.c
 *
 *  Created on: Jan 23, 2015
 *      Author: essdev
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <libusb-1.0/libusb.h>

#include "common.h"

static libusb_context *usb_ctx = NULL;
static int usb_intf_claimed = 0;

void hexdump(FILE * stream, void const * data, unsigned int len) {
	unsigned int i;
	unsigned int r, c;

	if (!stream)
		return;
	if (!data)
		return;

	for (r = 0, i = 0; r < (len / 16 + (len % 16 != 0)); r++, i += 16) {
		fprintf(stream, "%04X:   ", i); /* location of first byte in line */

		for (c = i; c < i + 8; c++) /* left half of hex dump */
			if (c < len)
				fprintf(stream, "%02X ", ((unsigned char const *) data)[c]);
			else
				fprintf(stream, "   "); /* pad if short line */

		fprintf(stream, "  ");

		for (c = i + 8; c < i + 16; c++) /* right half of hex dump */
			if (c < len)
				fprintf(stream, "%02X ", ((unsigned char const *) data)[c]);
			else
				fprintf(stream, "   "); /* pad if short line */

		fprintf(stream, "   ");

		for (c = i; c < i + 16; c++) /* ASCII dump */
			if (c < len)
				if (((unsigned char const *) data)[c] >= 32
						&& ((unsigned char const *) data)[c] < 127)
					fprintf(stream, "%c", ((char const *) data)[c]);
				else
					fprintf(stream, "."); /* put this for non-printables */
			else
				fprintf(stream, " "); /* pad if short line */

		fprintf(stream, "\n");
	}

	fflush(stream);
}

int usbInit(void) {
	int r;

	r = libusb_init(&usb_ctx);
	if (r) {
		ERR("Failed to initialize libusb: %s!\n", libusb_strerror(r));
		return r;
	}
	libusb_set_debug(usb_ctx, LIBUSB_LOG_LEVEL_ERROR);

	return 0;
}

void usbExit(void) {
	libusb_exit(usb_ctx);
}

int usbOpen(uint16_t vid, uint16_t pid, libusb_device_handle **dev_handle) {
	int r;
	int config;
	libusb_device_handle *dev;

	ASSERT(usb_ctx != NULL);

	dev = libusb_open_device_with_vid_pid(usb_ctx, vid, pid);
	if (! dev) {
		ERR("Failed to open USB device 0x%04X:0x%04X\n", vid, pid);
		return -1;
	}
	INF("Opened USB device 0x%04X:0x%04X\n", vid, pid);

	r = libusb_kernel_driver_active(dev, 0);
	if (r == 1) {
		DBG("Kernel driver is active for interface 0\n");
		r = libusb_detach_kernel_driver(dev, 0);
		if (r != LIBUSB_SUCCESS) {
			ERR("libusb_detach_kernel_driver failed: %s\n", libusb_strerror(r));
			return -1;
		}
	} else if (r == 0) {
		DBG("Kernel driver is NOT active for interface 0\n");
	} else {
		ERR("libusb_kernel_driver_active failed: %s\n", libusb_strerror(r));
		return -1;
	}

	r = libusb_get_configuration(dev, &config);
	if (r != LIBUSB_SUCCESS) {
		ERR("libusb_get_configuration failed: %s\n", libusb_strerror(r));
		return -1;
	}
	DBG("Current configuration is %d\n", config);

	r = libusb_claim_interface(dev, 0);
	if (r != LIBUSB_SUCCESS) {
		ERR("libusb_claim_interface failed: %s\n", libusb_strerror(r));
		return -1;
	}
	DBG("Claimed interface 0\n");

	*dev_handle = dev;
	usb_intf_claimed = 1;

	return 0;
}

int usbClose(libusb_device_handle *dev_handle) {
	int r;

	ENTER

	if (! dev_handle) {
		return 0;
	}

	if (usb_intf_claimed) {
		r = libusb_release_interface(dev_handle, 0);
		if (r != LIBUSB_SUCCESS) {
			ERR("libusb_release_interface failed: %s\n", libusb_strerror(r));
		}
		DBG("Released interface 0\n");
	}

	libusb_close(dev_handle);
	INF("Closed USB device\n");

	usb_intf_claimed = 0;

	LEAVE
	return 0;
}

int usbControl(libusb_device_handle *dev_handle,
		uint8_t bmRequestType,
		uint8_t bRequest,
		uint16_t wValue,
		uint16_t wIndex,
		unsigned char *data,
		uint16_t wLength,
		unsigned int timeout) {

	int r;

	ENTER

	r = libusb_control_transfer(
			dev_handle,					/* handle */
			bmRequestType, 				/* bmRequestType */
			bRequest,					/* bRequest */
			wValue,						/* wValue */
			wIndex,						/* wIndex */
			data,						/* data buffer */
			wLength,					/* data size */
			timeout						/* timeout */
			);
	if (r < 0) {
    	printf("libusb_control_transfer() failed (%d): %s\n", r, libusb_strerror(r));
	}

	HEXDUMP(stdout, data, wLength);

	LEAVE

	return r;
}

int usbBulk(libusb_device_handle *dev_handle,
		uint8_t endpoint,
		unsigned char *data,
		int wLength,
		int *wActualLength,
		unsigned int timeout) {

	int r;

	ENTER

	r = libusb_bulk_transfer(
			dev_handle,					/* handle */
			endpoint,		 			/* endpoint */
			data,						/* data buffer */
			wLength,					/* wLength */
			wActualLength,				/* wActualLength */
			timeout						/* timeout */
			);
	if (r < 0) {
    	printf("libusb_bulk_transfer() failed (%d): %s\n", r, libusb_strerror(r));
	}

	HEXDUMP(stdout, data, wLength);

	LEAVE

	return r;
}

/*
int usbFindDev(uint16_t vid, uint16_t pid) {
	libusb_device **devs;
	libusb_device *dev;
	int i = 0;
	int cnt;
	int found = 0;

	ASSERT(usb_ctx != NULL);

	cnt = libusb_get_device_list(usb_ctx, &devs);
	if (cnt < 0)
		return (int) cnt;

	while ((dev = devs[i++]) != NULL) {
		struct libusb_device_descriptor desc;
		int r = libusb_get_device_descriptor(dev, &desc);
		if (r < 0) {
			ERR("Failed to get device descriptor");
			found = r;
			break;
		}

		DBG("%04x:%04x (bus %d, device %d)\n",
			desc.idVendor, desc.idProduct,
			libusb_get_bus_number(dev), libusb_get_device_address(dev));

		if ((desc.idVendor == vid) && (desc.idProduct == pid)) {
			DBG("Found USB device %04x:%04x\n", vid, pid);
			found = 1;
			break;
		}
	}

	libusb_free_device_list(devs, 1);
	return found;
}
*/

int usbFindDevices(uint16_t vid, uint16_t *pids, int size) {
	libusb_device **devs;
	libusb_device *dev;
	int i = 0;
	int cnt;
	int found = 0;
	struct libusb_device_descriptor desc;
	int r;

	ASSERT(usb_ctx != NULL);

	cnt = libusb_get_device_list(usb_ctx, &devs);
	if (cnt < 0) {
		return cnt;
	}

	while ((dev = devs[i++]) != NULL) {
		r = libusb_get_device_descriptor(dev, &desc);
		if (r < 0) {
			ERR("Failed to get device descriptor");
			found = r;
			break;
		}

		DBG("%04x:%04x (bus %d, device %d)\n",
			desc.idVendor, desc.idProduct,
			libusb_get_bus_number(dev), libusb_get_device_address(dev));

		if (desc.idVendor == vid) {
			DBG("Found USB device %04x:%04x\n", desc.idVendor, desc.idProduct);
			if (found < size) {
				*(pids + found) = desc.idProduct;
				found++;
			} else {
				ERR("Found too many USB devices!\n");
				break;
			}
		}
	}

	libusb_free_device_list(devs, 1);
	return found;
}

int usbGetStrings(uint16_t vid, uint16_t pid, char *manf, char *model, char *serial, int size) {
	int r;
	libusb_device_handle *dev = NULL;

	r = usbOpen(vid, pid, &dev);
	if (r) {
		ERR("usbOpen() failed\n");
		return -1;
	}

	/* If open was successful then the firmware was properly downloaded. */


	r = libusb_get_string_descriptor_ascii(dev, 1, (unsigned char *)manf, size);
	if (r < 0) {
		ERR("failed to get manufacturer name\n");
    	usbClose(dev);
    	return -1;
	}

	r = libusb_get_string_descriptor_ascii(dev, 2, (unsigned char *)model, size);
	if (r < 0) {
		ERR("failed to get model name\n");
    	usbClose(dev);
    	return -1;
	}

	r = libusb_get_string_descriptor_ascii(dev, 3, (unsigned char *)serial, size);
	if (r < 0) {
		ERR("failed to get serial number\n");
    	usbClose(dev);
    	return -1;
	}

	r = usbClose(dev);
	if (r) {
		ERR("usbClose() failed\n");
		return -1;
	}
	dev = NULL;

	return 0;
}
