/*
 * fakevisa.c
 *
 *  Created on: Jan 23, 2015
 *      Author: essdev
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libusb-1.0/libusb.h>
#include <pthread.h>

#define NIVISA_USB
#include "visa.h"

#include "common.h"
#include "fwload.h"
#include "slre.h"

#define RESOURCE_LIST_TAG		0x00010000
#define SESSION_TAG				0x00020000
#define RES_MANAGER_TAG			0x00040000

#define MAX_RESOURCES			10

static int _session_index = 0;

typedef struct _fakeVisaResource {
	int is_valid;
	int has_session;
	ViAttrState manf_id;
	char manf_name[VI_FIND_BUFLEN];
	ViAttrState model_code;
	char model_name[VI_FIND_BUFLEN];
	ViAttrState tmo_value;
	char usb_serial_num[VI_FIND_BUFLEN];
	char rsrc[VI_FIND_BUFLEN];
} fakeVisaResource;

typedef struct _fakeVisaResourceList {
	int tag;
	int idx;
	ViSession vi;
	fakeVisaResource rsrcs[MAX_RESOURCES];
	struct _fakeVisaResourceList *next;
} fakeVisaResourceList;

typedef struct {
	int is_init;
	int tag;
	ViSession vi;
	ViAttrState session;
	char usb_fw_dir[VI_FIND_BUFLEN];
	fakeVisaResource rsrcs[MAX_RESOURCES];
} fakeVisaResourceManager;

typedef struct _fakeVisaSession {
	int tag;
	ViSession vi;
	libusb_device_handle *usb_handle;
	ViAttrState user_data;
	fakeVisaResource rsrc;
	struct _fakeVisaSession *next;
} fakeVisaSession;

// single resource manager
static fakeVisaResourceManager _resource_manager = {0};
// fake resource lists will be stored in this linked list
static fakeVisaResourceList *_resource_lists = NULL;
// fake sessions will be stored in this linked list
static fakeVisaSession *_sessions = NULL;
// protect access to the resource manager and sessions
static pthread_mutex_t mymutex = PTHREAD_MUTEX_INITIALIZER;

typedef struct
{
   ViStatus err;
   ViString descr;
} visa_errDescrStat_t;

static const visa_errDescrStat_t VISA_errDescrStat[] =
{
		{VI_SUCCESS, 					"Operation completed successfully."},
		{VI_SUCCESS_EVENT_EN, 			"Specified event is already enabled for at least one of the specified mechanisms."},
		{VI_SUCCESS_EVENT_DIS, 			"Specified event is already disabled for at least one of the specified mechanisms."},
		{VI_SUCCESS_QUEUE_EMPTY, 		"Operation completed successfully, but queue was already empty."},
		{VI_SUCCESS_TERM_CHAR, 			"The specified termination character was read."},
		{VI_SUCCESS_MAX_CNT, 			"The number of bytes transferred is equal to the requested input count. More data may be available."},
		{VI_SUCCESS_DEV_NPRESENT, 		"Session opened successfully, but the device at the specified address is not responding."},
		{VI_SUCCESS_TRIG_MAPPED, 		"The path from trigSrc to trigDest is already mapped."},
		{VI_SUCCESS_QUEUE_NEMPTY, 		"Wait terminated successfully on receipt of an event notification. There is at least one more event object of the requested type(s) available for this session."},
		{VI_SUCCESS_NCHAIN, 			"Event handled successfully. Do not invoke any other handlers on this session for this event."},
		{VI_SUCCESS_NESTED_SHARED, 		"Operation completed successfully, and this session has nested shared locks."},
		{VI_SUCCESS_NESTED_EXCLUSIVE, 	"Operation completed successfully, and this session has nested exclusive locks."},
		{VI_SUCCESS_SYNC, 				"Operation completed successfully, but the operation was actually synchronous rather than asynchronous."},
		{VI_WARN_QUEUE_OVERFLOW, 		"VISA received more event information of the specified type than the configured queue size could hold."},
		{VI_WARN_CONFIG_NLOADED, 		"The specified configuration either does not exist or could not be loaded. VISA-specified defaults will be used."},
		{VI_WARN_NULL_OBJECT, 			"The specified object reference is uninitialized."},
		{VI_WARN_NSUP_ATTR_STATE, 		"Although the specified state of the attribute is valid, it is not supported by this implementation."},
		{VI_WARN_UNKNOWN_STATUS, 		"The status code passed to the operation could not be interpreted."},
		{VI_WARN_NSUP_BUF, 				"The specified I/O buffer type is not supported."},
		{VI_WARN_EXT_FUNC_NIMPL, 		"The operation succeeded, but a lower level driver did not implement the extended functionality."},
		{VI_ERROR_SYSTEM_ERROR, 		"Unknown system error (miscellaneous error)."},
		{VI_ERROR_INV_OBJECT, 			"The given session or object reference is invalid."},
		{VI_ERROR_RSRC_LOCKED, 			"Specified type of lock cannot be obtained, or specified operation cannot be performed, because the resource is locked."},
		{VI_ERROR_INV_EXPR, 			"Invalid expression specified for search."},
		{VI_ERROR_RSRC_NFOUND, 			"Insufficient location information or the requested device or resource is not present in the system."},
		{VI_ERROR_INV_RSRC_NAME, 		"Invalid resource reference specified. Parsing error."},
		{VI_ERROR_INV_ACC_MODE, 		"Invalid access mode."},
		{VI_ERROR_TMO, 					"Timeout expired before operation completed."},
		{VI_ERROR_CLOSING_FAILED, 		"The VISA driver failed to properly close the session or object reference. This might be due to an error freeing internal or OS resources, a failed network connection, or a lower-level driver or OS error."},
		{VI_ERROR_INV_DEGREE, 			"Specified degree is invalid."},
		{VI_ERROR_INV_JOB_ID, 			"Specified job identifier is invalid."},
		{VI_ERROR_NSUP_ATTR, 			"The specified attribute is not defined or supported by the referenced object."},
		{VI_ERROR_NSUP_ATTR_STATE, 		"The specified state of the attribute is not valid, or is not supported as defined by the object."},
		{VI_ERROR_ATTR_READONLY, 		"The specified attribute is read-only."},
		{VI_ERROR_INV_LOCK_TYPE, 		"The specified type of lock is not supported by this resource."},
		{VI_ERROR_INV_ACCESS_KEY, 		"The access key to the resource associated with the specified session is invalid."},
		{VI_ERROR_INV_EVENT, 			"Specified event type is not supported by the resource."},
		{VI_ERROR_INV_MECH, 			"Invalid mechanism specified."},
		{VI_ERROR_HNDLR_NINSTALLED, 	"A handler was not installed."},
		{VI_ERROR_INV_HNDLR_REF, 		"The given handler reference is either invalid or was not installed."},
		{VI_ERROR_INV_CONTEXT, 			"Specified event context is invalid."},
		{VI_ERROR_QUEUE_OVERFLOW, 		"The event queue for the specified type has overflowed (usually due to previous events not having been closed)."},
		{VI_ERROR_NENABLED, 			"You must be enabled for events of the specified type in order to receive them."},
		{VI_ERROR_ABORT, 				"User abort occurred during transfer."},
		{VI_ERROR_RAW_WR_PROT_VIOL, 	"Violation of raw write protocol occurred during transfer."},
		{VI_ERROR_RAW_RD_PROT_VIOL, 	"Violation of raw read protocol occurred during transfer."},
		{VI_ERROR_OUTP_PROT_VIOL, 		"Device reported an output protocol error during transfer."},
		{VI_ERROR_INP_PROT_VIOL, 		"Device reported an input protocol error during transfer."},
		{VI_ERROR_BERR, 				"Bus error occurred during transfer."},
		{VI_ERROR_IN_PROGRESS, 			"Unable to queue the asynchronous operation because there is already an operation in progress."},
		{VI_ERROR_INV_SETUP, 			"Unable to start operation because setup is invalid (usually due to attributes being set to an inconsistent state)."},
		{VI_ERROR_QUEUE_ERROR, 			"Unable to queue the asynchronous operation (usually due to the I/O completion event not being enabled or insufficient space in the session's queue)."},
		{VI_ERROR_ALLOC, 				"Insufficient system resources to perform necessary memory allocation."},
		{VI_ERROR_INV_MASK, 			"Invalid buffer mask specified."},
		{VI_ERROR_IO, 					"Could not perform operation because of I/O error."},
		{VI_ERROR_INV_FMT, 				"A format specifier in the format string is invalid."},
		{VI_ERROR_NSUP_FMT, 			"A format specifier in the format string is not supported."},
		{VI_ERROR_LINE_IN_USE, 			"The specified trigger line is currently in use."},
		{VI_ERROR_NSUP_MODE, 			"The specified mode is not supported by this VISA implementation."},
		{VI_ERROR_SRQ_NOCCURRED, 		"Service request has not been received for the session."},
		{VI_ERROR_INV_SPACE, 			"Invalid address space specified."},
		{VI_ERROR_INV_OFFSET, 			"Invalid offset specified."},
		{VI_ERROR_INV_WIDTH, 			"Invalid access width specified."},
		{VI_ERROR_NSUP_OFFSET, 			"Specified offset is not accessible from this hardware."},
		{VI_ERROR_NSUP_VAR_WIDTH, 		"Cannot support source and destination widths that are different."},
		{VI_ERROR_WINDOW_NMAPPED, 		"The specified session is not currently mapped."},
		{VI_ERROR_RESP_PENDING, 		"A previous response is still pending, causing a multiple query error."},
		{VI_ERROR_NLISTENERS, 			"No listeners condition is detected (both NRFD and NDAC are deasserted)."},
		{VI_ERROR_NCIC, 				"The interface associated with this session is not currently the controller in charge."},
		{VI_ERROR_NSYS_CNTLR, 			"The interface associated with this session is not the system controller."},
		{VI_ERROR_NSUP_OPER, 			"The given session or object reference does not support this operation."},
		{VI_ERROR_INTR_PENDING, 		"An interrupt is still pending from a previous call."},
		{VI_ERROR_ASRL_PARITY, 			"A parity error occurred during transfer."},
		{VI_ERROR_ASRL_FRAMING, 		"A framing error occurred during transfer."},
		{VI_ERROR_ASRL_OVERRUN, 		"An overrun error occurred during transfer. A character was not read from the hardware before the next character arrived."},
		{VI_ERROR_TRIG_NMAPPED, 		"The path from trigSrc to trigDest is not currently mapped."},
		{VI_ERROR_NSUP_ALIGN_OFFSET, 	"The specified offset is not properly aligned for the access width of the operation."},
		{VI_ERROR_USER_BUF, 			"A specified user buffer is not valid or cannot be accessed for the required size."},
		{VI_ERROR_RSRC_BUSY, 			"The resource is valid, but VISA cannot currently access it."},
		{VI_ERROR_NSUP_WIDTH, 			"Specified width is not supported by this hardware."},
		{VI_ERROR_INV_PARAMETER, 		"The value of some parameter (which parameter is not known) is invalid."},
		{VI_ERROR_INV_PROT, 			"The protocol specified is invalid."},
		{VI_ERROR_INV_SIZE, 			"Invalid size of window specified."},
		{VI_ERROR_WINDOW_MAPPED, 		"The specified session currently contains a mapped window."},
		{VI_ERROR_NIMPL_OPER, 			"The given operation is not implemented."},
		{VI_ERROR_INV_LENGTH, 			"Invalid length specified."},
		{VI_ERROR_INV_MODE, 			"Invalid mode specified."},
		{VI_ERROR_SESN_NLOCKED, 		"The current session did not have a lock on the resource."},
		{VI_ERROR_MEM_NSHARED, 			"The device does not export any memory."},
		{VI_ERROR_LIBRARY_NFOUND, 		"A code library required by VISA could not be located or loaded."},
		{VI_ERROR_NSUP_INTR, 			"The interface cannot generate an interrupt on the requested level or with the requested statusID value."},
		{VI_ERROR_INV_LINE, 			"The value specified by the line parameter is invalid."},
		{VI_ERROR_FILE_ACCESS, 			"An error occurred while trying to open the specified file. Possible reasons include an invalid path or lack of access rights."},
		{VI_ERROR_FILE_IO, 				"An error occurred while performing I/O on the specified file."},
		{VI_ERROR_NSUP_LINE, 			"One of the specified lines (trigSrc or trigDest) is not supported by this VISA implementation, or the combination of lines is not a valid mapping."},
		{VI_ERROR_NSUP_MECH, 			"The specified mechanism is not supported for the given event type."},
		{VI_ERROR_INTF_NUM_NCONFIG, 	"The interface type is valid but the specified interface number is not configured."},
		{VI_ERROR_CONN_LOST, 			"The connection for the given session has been lost."},
		{VI_ERROR_MACHINE_NAVAIL, 		"The remote machine does not exist or is not accepting any connections. If the NI-VISA server is installed and running on the remote machine, it may have an incompatible version or may be listening on a different port."},
		{VI_ERROR_NPERMISSION, 			"Access to the resource or remote machine is denied. This is due to lack of sufficient privileges for the current user or machine"},
		{VI_USB_PIPE_STATE_UNKNOWN,		"USB pipe status is unknown."}
};

static fakeVisaSession *findFakeViSession (ViObject vi) {
	fakeVisaSession *ses;

	if (vi == VI_NULL) {
		ERR("Invalid ViObject\n");
		return NULL;
	}

	if ((vi & SESSION_TAG) == 0) {
		ERR("Not a session ViObject\n");
		return NULL;
	}

	ses = _sessions;
	if (ses == NULL) {
		return NULL;
	}
	while (ses) {
		if (ses->vi == vi) {
			DBG("Found session for object '0x%X'\n", (unsigned int)vi);
			return ses;
		}
		ses = ses->next;
	}

	ERR("Failed to find session for object '0x%X'\n", (unsigned int)vi);
	return NULL;
}

static fakeVisaResourceList *findFakeViResourceList (ViObject vi) {
	fakeVisaResourceList *res_list;

	if (vi == VI_NULL) {
		ERR("Invalid ViObject\n");
		return NULL;
	}

	if ((vi & RESOURCE_LIST_TAG) == 0) {
		ERR("Not a resource list ViObject\n");
		return NULL;
	}

	if (_resource_lists == NULL) {
		DBG("Resource list is empty\n");
		return NULL;
	}

	res_list = _resource_lists;
	while (res_list) {
		if (res_list->vi == vi) {
			DBG("Found resource list for object '0x%X'\n", vi);
			return res_list;
		}
		res_list = res_list->next;
	}

	ERR("Failed to find resource list for object '0x%X'\n", (unsigned int)vi);
	return NULL;
}

static fakeVisaResource *findFakeViResourceByPID (u_int16_t pid) {
	int i;
	fakeVisaResource *res;

	for (i = 0; i < MAX_RESOURCES; i++) {
		res = &_resource_manager.rsrcs[i];
		if (res->model_code == pid) {
			DBG("Found resource '%s' for PID '0x%04X'\n", res->rsrc, pid);
			return res;
		}
	}

	DBG("No resource found for PID '0x%04X'\n", pid);
	return NULL;
}

static fakeVisaResource *findFakeViResourceByRsrc (char *rsrc) {
	int i;
	fakeVisaResource *res;

	for (i = 0; i < MAX_RESOURCES; i++) {
		res = &_resource_manager.rsrcs[i];
		if (strncmp(rsrc, res->rsrc, VI_FIND_BUFLEN) == 0) {
			DBG("Found resource '%s'\n", res->rsrc);
			return res;
		}
	}

	DBG("No resource found for '%s'\n", rsrc);
	return NULL;
}

static void addFakeViResource (fakeVisaResource *res) {
	int i;
	fakeVisaResource *rm_res;

	for (i = 0; i < MAX_RESOURCES; i++) {
		rm_res = &_resource_manager.rsrcs[i];
		if (strncmp(rm_res->rsrc, res->rsrc, VI_FIND_BUFLEN) == 0) {
			DBG("Resource '%s' already in resource manager\n", res->rsrc);
			return;
		}
	}

	DBG("Adding resource '%s'\n", res->rsrc);
	for (i = 0; i < MAX_RESOURCES; i++) {
		rm_res = &_resource_manager.rsrcs[i];
		if (! rm_res->is_valid) {
			memcpy(rm_res, res, sizeof(fakeVisaResource));
			return;
		}
	}

	ERR("No more space in resource manager for new resource!\n");
	ASSERT(1 == 0);
}

ViStatus _VI_FUNC  viOpenDefaultRM (ViPSession vi) {
	int r;

	pthread_mutex_lock(&mymutex);

	// initialize resource manager once
	if (_resource_manager.is_init == 0) {
		_resource_manager.tag = RES_MANAGER_TAG;
		_resource_manager.is_init = 1;
		_resource_manager.vi = RES_MANAGER_TAG + _session_index;
		_session_index++;

		r = usbInit();
		if (r) {
			ERR("usbInit() failed\n");
			pthread_mutex_unlock(&mymutex);
			return -1;
		}
	}

	// dummy
	*vi = _resource_manager.vi;

	pthread_mutex_unlock(&mymutex);
	return VI_SUCCESS;
}

ViStatus _VI_FUNC  viFindRsrc      (ViSession sesn, ViString expr, ViPFindList vi,
                                    ViPUInt32 retCnt, ViChar _VI_FAR desc[]) {
	int r;
	char find[VI_FIND_BUFLEN];
	char val[VI_FIND_BUFLEN];
	struct slre_cap caps[10];
	int i, j = 0, str_len = strlen(expr);
	int vid = 0;
	int pid = 0;
	int cnt;
	u_int16_t pids[10];
	int nr_usb_devs;
	int d;
	fakeVisaResourceList *res_list;
	fakeVisaResource *res, *rm_res;

	cnt = 0;

	pthread_mutex_lock(&mymutex);

	res_list = calloc(1, sizeof(fakeVisaResourceList));
	res_list->tag = RESOURCE_LIST_TAG;
	res_list->vi = RESOURCE_LIST_TAG + _session_index;
	res_list->idx = 0;
	_session_index++;

	memset(caps, 0, sizeof(caps));

	/* find vendor id */
	sprintf(find, "VI_ATTR_MANF_ID==(0x[a-f0-9]+)");
	r = slre_match(find, expr, strlen(expr), caps, 10, SLRE_IGNORE_CASE);
	if (r < 0) {
		ERR("Regex for VID failed\n");
		goto end;
	}
	DBG("Checking VID: [%.*s]\n", caps[0].len, caps[0].ptr);
	sprintf(val, "%.*s", caps[0].len, caps[0].ptr);
	vid = strtol(val, NULL, 16);

	if (vid != TLCCS_VID) {
		ERR("Regex did not match Thorlabs VID 0x%04X\n", TLCCS_VID);
		goto end;
	}

	// Get a list of all PIDs for a given VID
	r = usbFindDevices(vid, pids, 10);
	if (r < 0) {
		ERR("Failed to get list of USB devices\n");
		goto end;
	}

	if (r == 0) {
		INF("No USB devices with VID 0x%04X found\n", vid);
		goto end;
	}
	nr_usb_devs = r;
	DBG("Found %d USB devices with VID 0x%04X\n", nr_usb_devs, vid);

	res = &res_list->rsrcs[0];

	/* find product id */
	sprintf(find, "VI_ATTR_MODEL_CODE==(0x[a-f0-9]+)");
	while (j < str_len) {
		i = slre_match(find, expr + j, str_len - j, caps, 2, SLRE_IGNORE_CASE);
		if (i == SLRE_NO_MATCH) {
			goto end;
		}
		if (i < 0) {
			ERR("Regex for PID failed with %d.\n", i);
			goto end;
		}
		DBG("Checking PID: [%.*s]\n", caps[0].len, caps[0].ptr);

		sprintf(val, "%.*s", caps[0].len, caps[0].ptr);
		pid = strtol(val, NULL, 16);

		// if PID already exists in the session list then skip this device
		rm_res = findFakeViResourceByPID(pid);
		if (rm_res) {
			memcpy(res, rm_res, sizeof(fakeVisaResource));
			DBG("Skipping PID 0x%04X..\n", pid);
			// store first resource in the resource list if pointer is provided
			if ((vi != VI_NULL) && (cnt == 0)) {
				strcpy(desc, res->rsrc);
				*vi = res_list->vi;
			}
			cnt++;
			if (cnt >= MAX_RESOURCES) {
				goto end;
			}
			// next resource in resource list
			res++;
		} else {
			for (d = 0; d < nr_usb_devs; d++) {
				ASSERT(res->is_valid == 0);

				if (*(pids + d) == (pid - 1)) {
					// found CCS device (has PID decremented by one) that needs firmware loading
					r = fwload(vid, pid - 1, _resource_manager.usb_fw_dir);
					if (r) {
						ERR("fwload() failed for 0x%04X:0x%04X\n", (ViUInt16 )vid, (ViUInt16 )pid - 1);
						continue;
					}
					DBG("Loaded firmware for CCS device! (0x%04X:0x%04X --> 0x%04X:0x%04X)\n",
							(ViUInt16 )vid, (ViUInt16 )pid - 1, (ViUInt16 )vid, (ViUInt16 )pid);

				} else if (*(pids + d) == pid) {
					// found CCS device
					DBG("CCS device 0x%04X:0x%04X\n", (ViUInt16 )vid, (ViUInt16 )pid);
				} else {
					continue;
				}

				r = usbGetStrings(vid, pid,
						res->manf_name,
						res->model_name,
						res->usb_serial_num,
						VI_FIND_BUFLEN);
				if (r) {
					ERR("Failed to get USB device strings!\n");
					continue;
				}

				/* setup session for found device */
				res->manf_id = vid;
				res->model_code = pid;
				res->tmo_value = 2000;
				// create resource string
				// USB[board]::manufacturer ID::modelcode::serial number[::USBinterfacenumber]::RAW
				sprintf(res->rsrc, "USB::0x%04X::0x%04X::%s::RAW",
						(int)res->manf_id, (int)res->model_code, res->usb_serial_num);
				res->is_valid = 1;
				res->has_session = 0;

				// store first resource in the resource list if pointer is provided
				if ((vi != VI_NULL) && (cnt == 0)) {
					strcpy(desc, res->rsrc);
					*vi = res_list->vi;
				}

				// add to resource manager
				addFakeViResource(res);

				cnt++;
				if (cnt >= MAX_RESOURCES) {
					goto end;
				}

				// next resource in resource list
				res++;

			} // for pids
		} // if rm_res

		/* move along the string */
		j += i;
	}

end:
	r = VI_ERROR_RSRC_NFOUND;
	if (cnt) {
		if (retCnt) {
			*retCnt = cnt;
		}
		DBG("Found %d CCS USB devices\n", cnt);
		r = VI_SUCCESS;
	} else {
		ERR("No CCS USB devices found\n");
	}

	if (vi == VI_NULL) {
		free(res_list);
	} else {
		if (_resource_lists == NULL) {
			_resource_lists = res_list;
		} else {
			fakeVisaResourceList *l = _resource_lists;
			while (l->next) {
				l = l->next;
			}
			l->next = res_list;
		}
	}

	pthread_mutex_unlock(&mymutex);
	return r;
}

ViStatus _VI_FUNC  viFindNext      (ViFindList vi, ViChar _VI_FAR desc[]) {
	fakeVisaResourceList *res_list;
	fakeVisaResource *res;
	int idx;

	if (vi == VI_NULL) {
		ERR("Invalid ViObject\n");
		return VI_ERROR_INV_OBJECT;
	}

	res_list = findFakeViResourceList(vi);
	if (res_list == NULL) {
		return VI_ERROR_RSRC_NFOUND;
	}
	idx = res_list->idx + 1;
	if (idx >= MAX_RESOURCES) {
		DBG("Next resource not available!\n");
		return VI_ERROR_RSRC_NFOUND;
	}
	res = &res_list->rsrcs[idx];

	if (res->is_valid == 0) {
		DBG("Next resource not valid!\n");
		return VI_ERROR_RSRC_NFOUND;
	}

	// we have another resource
	strcpy(desc, res->rsrc);

	// prepare for next call to viFindNext()
	res_list->idx = idx;

	return VI_SUCCESS;
}

ViStatus _VI_FUNC  viOpen          (ViSession sesn, ViRsrc name, ViAccessMode mode,
                                    ViUInt32 timeout, ViPSession vi) {
	int r;
	fakeVisaSession *ses, *curr_ses;
	fakeVisaResource *res;

	*vi = VI_NULL;

	pthread_mutex_lock(&mymutex);

	// find session by resource name
	ses = _sessions;
	if (ses) {
		while (ses) {
			if (strncmp(ses->rsrc.rsrc, name, VI_FIND_BUFLEN) == 0) {
				DBG("Session already opened for resource '%s'\n", name);
				return VI_ERROR_RSRC_BUSY;
			}
			ses = ses->next;
		}
	}

	// creating new session for resource
	res = findFakeViResourceByRsrc(name);
	if (! res) {
		return VI_ERROR_RSRC_NFOUND;
	}
	if (! res->is_valid) {
		return VI_ERROR_RSRC_NFOUND;
	}
	if (res->has_session) {
		return VI_ERROR_RSRC_BUSY;
	}

	res->has_session = 1;
	curr_ses = calloc(1, sizeof(fakeVisaSession));
	curr_ses->tag = SESSION_TAG;
	curr_ses->vi = SESSION_TAG + _session_index;
	_session_index++;
	curr_ses->next = NULL;
	memcpy(&curr_ses->rsrc, res, sizeof(fakeVisaResource));

	r = usbOpen(res->manf_id, res->model_code, &curr_ses->usb_handle);
	if (r) {
		ERR("usbOpen() failed\n");
		pthread_mutex_unlock(&mymutex);
		return r;
	}

	// save session to global list
	ses = _sessions;
	if (! ses) {
		_sessions = curr_ses;
	} else {
		while (ses->next) {
			ses = ses->next;
		}
		ses->next = curr_ses;
	}

	// used with other VI functions to access proper resource
	*vi = curr_ses->vi;

	pthread_mutex_unlock(&mymutex);
	return VI_SUCCESS;
}

ViStatus _VI_FUNC  viClose         (ViObject vi) {
	fakeVisaSession *ses, *ses_curr, *ses_prev;
	fakeVisaResourceList *res_list, *res_list_curr, *res_list_prev;
	fakeVisaResource *res;

	pthread_mutex_lock(&mymutex);

	if ((vi & RES_MANAGER_TAG) == RES_MANAGER_TAG) {

// XXX: For this resource manager needs to be destroyed last!
//		if (! _sessions) {
//			usbExit();
//		}
		goto ok;
	}

	if ((vi & RESOURCE_LIST_TAG) == RESOURCE_LIST_TAG) {
		res_list = _resource_lists;
		// is resource list empty?
		if (! res_list) {
			goto fail;
		}

		res_list_prev = NULL;
		while (res_list) {
			if (res_list->vi == vi) {
				res_list_curr = res_list;
				if (res_list_prev) {
					res_list_prev->next = res_list->next;
				} else if (res_list == _resource_lists) {
					_resource_lists = res_list->next;
				}
				free(res_list_curr);
				goto ok;
			} else {
				res_list_prev = res_list;
			}
			res_list = res_list->next;
		}

		goto fail;
	}

	if ((vi & SESSION_TAG) == SESSION_TAG) {
		ses = _sessions;
		// is session list empty?
		if (! ses) {
			goto fail;
		}

		ses_prev = NULL;
		while (ses) {
			if (ses->vi == vi) {
				ses_curr = ses;
				if (ses_prev) {
					ses_prev->next = ses->next;
				} else if (ses == _sessions) {
					_sessions = ses->next;
				}
				usbClose(ses_curr->usb_handle);
				res = findFakeViResourceByRsrc(ses_curr->rsrc.rsrc);
				if (res) {
					// resource can have new session
					res->has_session = 0;
				}
				free(ses_curr);
				goto ok;
			} else {
				ses_prev = ses;
			}
			ses = ses->next;
		}

		goto fail;
	}

fail:
	pthread_mutex_unlock(&mymutex);
	ERR("Invalid object!\n");
	return VI_ERROR_INV_OBJECT;

ok:
	pthread_mutex_unlock(&mymutex);
	DBG("Object closed!\n");
	return VI_SUCCESS;
}

ViStatus _VI_FUNC  viSetAttribute  (ViObject vi, ViAttr attrName, ViAttrState attrValue) {
	fakeVisaSession *ses;

	ses = findFakeViSession(vi);
	if (ses == VI_NULL) {
		return VI_ERROR_RSRC_NFOUND;
	}

	// XXX: Are there any other attributes to take care of?
	switch (attrName) {
	case VI_ATTR_USER_DATA:
		ses->user_data = attrValue;
		break;
	case VI_ATTR_RM_SESSION:
		_resource_manager.session = attrValue;
		break;
	case VI_ATTR_MANF_ID:
		ses->rsrc.manf_id = attrValue;
		break;
	case VI_ATTR_MANF_NAME:
		strncpy(ses->rsrc.manf_name, (char *)attrValue, VI_FIND_BUFLEN);
		break;
	case VI_ATTR_MODEL_CODE:
		ses->rsrc.model_code = attrValue;
		break;
	case VI_ATTR_MODEL_NAME:
		strncpy(ses->rsrc.model_name, (char *)attrValue, VI_FIND_BUFLEN);
		break;
	case VI_ATTR_USB_SERIAL_NUM:
		strncpy(ses->rsrc.usb_serial_num, (char *)attrValue, VI_FIND_BUFLEN);
		break;
	case VI_ATTR_TMO_VALUE:
		ses->rsrc.tmo_value = attrValue;
		break;
	default:
		return VI_ERROR_NSUP_ATTR;
	}
	return VI_SUCCESS;
}

ViStatus _VI_FUNC  viGetAttribute  (ViObject vi, ViAttr attrName, void _VI_PTR attrValue) {
	fakeVisaSession *ses;

	ses = findFakeViSession(vi);
	if (ses == VI_NULL) {
		return VI_ERROR_RSRC_NFOUND;
	}

	// XXX: Are there any other attributes to take care of?
	switch (attrName) {
	case VI_ATTR_USER_DATA:
		*(ViAttrState *)attrValue = ses->user_data;
		break;
	case VI_ATTR_RM_SESSION:
		*(ViAttrState *)attrValue = _resource_manager.session;
		break;
	case VI_ATTR_MANF_ID:
		*(ViAttrState *)attrValue = ses->rsrc.manf_id;
		break;
	case VI_ATTR_MANF_NAME:
		strncpy((char *)attrValue, ses->rsrc.manf_name, VI_FIND_BUFLEN);
		break;
	case VI_ATTR_MODEL_CODE:
		*(ViAttrState *)attrValue = ses->rsrc.model_code;
		break;
	case VI_ATTR_MODEL_NAME:
		strncpy((char *)attrValue, ses->rsrc.model_name, VI_FIND_BUFLEN);
		break;
	case VI_ATTR_USB_SERIAL_NUM:
		strncpy((char *)attrValue, ses->rsrc.usb_serial_num, VI_FIND_BUFLEN);
		break;
	case VI_ATTR_TMO_VALUE:
		*(ViAttrState *)attrValue = ses->rsrc.tmo_value;
		break;
	default:
		return VI_ERROR_NSUP_ATTR;
	}
	return VI_SUCCESS;
}

ViStatus _VI_FUNC  viStatusDesc    (ViObject vi, ViStatus status, ViChar _VI_FAR desc[]) {
	const visa_errDescrStat_t *ptr;

	if (!desc) {
		return VI_ERROR_INV_PARAMETER;
	}

	// VISA errors
	ptr = VISA_errDescrStat;
	while (ptr->descr != VI_NULL) {
		if (ptr->err == status) {
			strcpy(desc, ptr->descr);
			return (VI_SUCCESS);
		}
		ptr++;
	}
	// Not found
	return VI_WARN_UNKNOWN_STATUS;
}

ViStatus _VI_FUNC  viRead          (ViSession vi, ViPBuf buf, ViUInt32 cnt, ViPUInt32 retCnt) {
	int r;
	// XXX: do not hardcode here!
	uint8_t endpoint = 0x86;
	fakeVisaSession *sesPtr;

	sesPtr = findFakeViSession(vi);
	if (sesPtr == VI_NULL) {
		return VI_ERROR_RSRC_NFOUND;
	}

	r = usbBulk(sesPtr->usb_handle, endpoint, buf, cnt, (int *)retCnt, 1000);
	if (r < 0) {
		return VI_USB_PIPE_STATE_UNKNOWN;
	}

	return VI_SUCCESS;
}

ViStatus _VI_FUNC  viClear         (ViSession vi) {
	// XXX: Do something here?
	return VI_SUCCESS;
}

ViStatus _VI_FUNC  viFlush         (ViSession vi, ViUInt16 mask) {
	// XXX: Do something here?
	return VI_SUCCESS;
}

ViStatus _VI_FUNC  viUsbControlOut (ViSession vi, ViInt16 bmRequestType, ViInt16 bRequest,
                                    ViUInt16 wValue, ViUInt16 wIndex, ViUInt16 wLength,
                                    ViBuf buf) {
	int r;
	fakeVisaSession *sesPtr;

	sesPtr = findFakeViSession(vi);
	if (sesPtr == VI_NULL) {
		return VI_ERROR_RSRC_NFOUND;
	}

	r = usbControl(sesPtr->usb_handle, bmRequestType, bRequest, wValue, wIndex, buf, wLength, 1000);
	if (r < 0) {
		return VI_USB_PIPE_STATE_UNKNOWN;
	}

	return VI_SUCCESS;
}

ViStatus _VI_FUNC  viUsbControlIn  (ViSession vi, ViInt16 bmRequestType, ViInt16 bRequest,
                                    ViUInt16 wValue, ViUInt16 wIndex, ViUInt16 wLength,
                                    ViPBuf buf, ViPUInt16 retCnt) {
	int r;
	fakeVisaSession *sesPtr;

	sesPtr = findFakeViSession(vi);
	if (sesPtr == VI_NULL) {
		return VI_ERROR_RSRC_NFOUND;
	}

	r = usbControl(sesPtr->usb_handle, bmRequestType, bRequest, wValue, wIndex, buf, wLength, 1000);
	if (r < 0) {
		return VI_USB_PIPE_STATE_UNKNOWN;
	}

	*retCnt = r;
	return VI_SUCCESS;
}

/* This is *NOT* a VISA function. */
ViStatus _VI_FUNC  viSetFirmwareFolder          (ViSession vi, ViString fw_dir) {

	if (vi == VI_NULL) {
		return VI_ERROR_INV_OBJECT;
	}
	if (vi != _resource_manager.vi) {
		return VI_ERROR_INV_CONTEXT;
	}
	strncpy((char *)_resource_manager.usb_fw_dir, fw_dir, VI_FIND_BUFLEN);
	return VI_SUCCESS;
}
