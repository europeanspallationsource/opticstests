/*
 * fwload.h
 *
 *  Created on: Jan 25, 2015
 *      Author: hinxx
 */

#ifndef FWLOAD_H_
#define FWLOAD_H_

int fwload(uint16_t vid, uint16_t pid, char *fwPath);

#endif /* FWLOAD_H_ */
