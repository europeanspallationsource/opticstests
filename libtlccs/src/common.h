/*
 * common.h
 *
 *  Created on: Jan 23, 2015
 *      Author: essdev
 */

#ifndef COMMON_H_
#define COMMON_H_


#include <stdarg.h>
#include <stdio.h>
#include <assert.h>

#include <libusb-1.0/libusb.h>


// See Makefile
#define TLCCSDEBUG		_TLCCSDEBUG


#define INF(format, ...) \
	do { if (1) \
		fprintf(stderr, \
				"INF %s:%d:%s(): " \
				format, \
				__FILE__, \
				__LINE__, \
				__func__, \
				## __VA_ARGS__); \
	} while (0)

#define DBG(format, ...) \
	do { if (TLCCSDEBUG) \
		fprintf(stderr, \
				"DBG %s:%d:%s(): " \
				format, \
				__FILE__, \
				__LINE__, \
				__func__, \
				## __VA_ARGS__); \
	} while (0)

#define ERR(format, ...) \
	fprintf(stderr, \
			"ERR %s:%d:%s(): " \
			format, \
			__FILE__, \
			__LINE__, \
			__func__, \
			## __VA_ARGS__)

#define ASSERT(x) \
	do { if (TLCCSDEBUG) \
		assert(x); \
	} while(0)

#define ENTER	DBG("Enter >>>\n");
#define LEAVE	DBG("Leave <<<\n");

/*---------------------------------------------------------------------------
 USB VIDs and PIDs
---------------------------------------------------------------------------*/
#define TLCCS_VID                   0x1313   // Thorlabs
#define CCS100_PID                  0x8081   // CCS100 Compact Spectrometer
#define CCS125_PID                  0x8083   // CCS125 Special Spectrometer
#define CCS150_PID                  0x8085   // CCS150 UV Spectrometer
#define CCS175_PID                  0x8087   // CCS175 NIR Spectrometer
#define CCS200_PID                  0x8089   // CCS200 UV-NIR Spectrometer

void hexdump(FILE * stream, void const * data, unsigned int len);
#define	HEXDUMP(stream, data, len) do { \
	if (TLCCSDEBUG > 1) \
	hexdump(stream, data, len); \
} while(0)

/*
 * USB
 */
int usbInit(void);
void usbExit(void);
int usbOpen(uint16_t vid, uint16_t pid, libusb_device_handle **dev_handle);
int usbClose(libusb_device_handle *dev_handle);
int usbControl(libusb_device_handle *dev_handle,
		uint8_t bmRequestType,
		uint8_t bRequest,
		uint16_t wValue,
		uint16_t wIndex,
		unsigned char *data,
		uint16_t wLength,
		unsigned int timeout);
int usbBulk(libusb_device_handle *dev_handle,
		uint8_t endpoint,
		unsigned char *data,
		int wLength,
		int *wActualLength,
		unsigned int timeout);
//int usbFindDev(uint16_t vid, uint16_t pid);
int usbFindDevices(uint16_t vid, uint16_t *pids, int size);
int usbGetStrings(uint16_t vid, uint16_t pid, char *manf, char *model, char *serial, int size);

#endif /* COMMON_H_ */
