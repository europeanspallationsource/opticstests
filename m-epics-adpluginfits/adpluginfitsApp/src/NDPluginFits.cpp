/*
 * NDPluginFits.cpp
 *
 * Image statistics and profile fitting plugin
 * Author: Mark Rivers, Hinko Kocevar
 *
 * Created March 14, 2016
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include <epicsString.h>
#include <epicsMutex.h>
#include <iocsh.h>

#include <asynDriver.h>

#include <epicsExport.h>
#include "NDPluginDriver.h"
#include "NDPluginFits.h"

#include <lmcurve.h>

static const char *driverName="NDPluginFits";


/* model function: a 3-peak gauss */
double f(double t, const double *p) {
	double B = p[0];
	double A1 = p[1];
	double mu1 = p[2];
	double sigma1 = p[3];
	double A2 = p[4];
	double mu2 = p[5];
	double sigma2 = p[6];
	double A3 = p[7];
	double mu3 = p[8];
	double sigma3 = p[9];
	double term1 = (t - mu1) / sigma1;
	double term2 = (t - mu2) / sigma2;
	double term3 = (t - mu3) / sigma3;
	double fi = 0;

	fi += B;
	fi += A1 * exp(-0.5 * term1 * term1);
	fi += A2 * exp(-0.5 * term2 * term2);
	fi += A3 * exp(-0.5 * term3 * term3);

	//printf("t %.9f fi %.9g ", t, fi);
	return fi;
}

template <typename epicsType>
asynStatus NDPluginFits::doComputeFitsT(NDArray *pArray)
{
	epicsInt32 i, j;

    if (pArray->ndims > 2) return(asynError);

    this->unlock();

    NDArrayInfo arrayInfo;
    pArray->getInfo(&arrayInfo);

    size_t sizeX = 0;
    size_t sizeY = 0;
    if (pArray->ndims > 0) sizeX = pArray->dims[0].size;
    if (pArray->ndims == 1) sizeY = 1;
    if (pArray->ndims > 1)  sizeY = pArray->dims[1].size;
    //printf("pArray size X,Y: %ld %ld\n", sizeX, sizeY);

    if (sizeX == 0 || sizeY > 1) {
        printf("Incorrect ROI settings, need X x 1 array! Fit may not be possible!\n");
    }

    epicsType *pData = (epicsType *)pArray->pData;
	for (i = 0; i < this->sizeX; i++) {
		this->dataSet[dataSetX][i] = i;
		this->dataSet[dataSetFit][i] = 0.0;
		this->dataSet[dataSetY][i] = (double)pData[i];
	}
	double *t = this->dataSet[dataSetX];
	double *y = this->dataSet[dataSetY];
	double *fit = this->dataSet[dataSetFit];

    /* data points size */
	int m = this->sizeX;
	/* number of parameters in model function f */
	int n = this->nrFitParam;

	/* always grab starting values from user input */
	memcpy(this->fitParam, this->initialFitParam, this->nrFitParam * sizeof(double));
    double *p = this->fitParam;

//	printf("\nInitial params: ");
//	for (i = 0; i < n; ++i) {
//		printf("%.7g ", p[i]);
//	}
//	printf("\n\n");
//	printf("\nInitial t: ");
//	for (i = 0; i < m; ++i) {
//		printf("%.7g ", t[i]);
//	}
//	printf("\n\n");
//
//	printf("\nInitial y: ");
//	for (i = 0; i < m; ++i) {
//		printf("%.7g ", y[i]);
//	}
//	printf("\n\n");

    lm_control_struct control = lm_control_double;
    lm_status_struct status;
    //control.verbosity = 9;
    control.verbosity = 0;

//    printf("Fitting ...\n" );
    /* now the call to lmfit */
    lmcurve(n, p, m, t, y, f, &control, &status);

//    printf("Results:\n" );
//    printf("status after %d function evaluations:\n  %s\n",
//            status.nfev, lm_infmsg[status.outcome] );
//
//    printf("obtained parameters:\n");
//    for (i = 0; i < n; ++i)
//        printf("  par[%i] = %12g\n", i, p[i]);
//    printf("obtained norm:\n  %12g\n", status.fnorm );
//
//    printf("fitting data as follows:\n");
//    for (i = 0; i < m; ++i) {
//        printf("  t[%2d]=%4g y=%6g fit=%10g residue=%12g\n",
//                i, t[i], y[i], f(t[i],p), y[i] - f(t[i],p));
//    }

    /* see lmfit.c for outcome codes; positive <4 is success, other failure */
    if ((status.nfev > 0) && (status.outcome >= 0 && status.outcome <= 3)) {
    	printf("lmfit() success!\n");
        for (i = 0; i < m; ++i) {
        	fit[i] = f(t[i],p);
        }
    	/* update fitted values */
        setDoubleParam(NDPluginFitsBackground, p[0]);
    	for (i = 0, j = 1; i < this->maxPeaks; i++, j += 3) {
    		setDoubleParam(i, NDPluginFitsPeakAmplitude, p[j]);
    		setDoubleParam(i, NDPluginFitsPeakMu, p[j+1]);
    		setDoubleParam(i, NDPluginFitsPeakSigma, p[j+2]);
    		printf("RESULT PEAK#%d max: %f, center %f, sigma %f\n", i+1, p[j], p[j+1], p[j+2]);
    		callParamCallbacks(i);
    	}
    } else {
    	printf("lmfit() fail!\n");
        for (i = 0; i < m; ++i) {
        	fit[i] = 0.0;
        }
    }

    this->lock();
    doCallbacksFloat64Array(this->dataSet[dataSetY],   this->sizeX, NDPluginFitsY, 0);
    doCallbacksFloat64Array(this->dataSet[dataSetX],   this->sizeX, NDPluginFitsX, 0);
    doCallbacksFloat64Array(this->dataSet[dataSetFit], this->sizeX, NDPluginFitsFit, 0);

    return(asynSuccess);
}

asynStatus NDPluginFits::doComputeFits(NDArray *pArray)
{
    asynStatus status;

    switch(pArray->dataType) {
        case NDInt8:
            status = doComputeFitsT<epicsInt8>(pArray);
            break;
        case NDUInt8:
            status = doComputeFitsT<epicsUInt8>(pArray);
            break;
        case NDInt16:
            status = doComputeFitsT<epicsInt16>(pArray);
            break;
        case NDUInt16:
            status = doComputeFitsT<epicsUInt16>(pArray);
            break;
        case NDInt32:
            status = doComputeFitsT<epicsInt32>(pArray);
            break;
        case NDUInt32:
            status = doComputeFitsT<epicsUInt32>(pArray);
            break;
        case NDFloat32:
            status = doComputeFitsT<epicsFloat32>(pArray);
            break;
        case NDFloat64:
            status = doComputeFitsT<epicsFloat64>(pArray);
            break;
        default:
            status = asynError;
        break;
    }
    return(status);
}

/** Callback function that is called by the NDArray driver with new NDArray data.
  * \param[in] pArray  The NDArray from the callback.
  */
void NDPluginFits::processCallbacks(NDArray *pArray)
{
    /* It is called with the mutex already locked.
     * It unlocks it during long calculations when private structures don't
     * need to be protected.
     */
    size_t sizeX=0;
    int i;
    NDArrayInfo arrayInfo;
    static const char* functionName = "processCallbacks";

    /* Call the base class method */
    NDPluginDriver::processCallbacks(pArray);

    pArray->getInfo(&arrayInfo);

    if (pArray->ndims > 0) sizeX = pArray->dims[0].size;

    if (sizeX != (size_t)this->sizeX) {
        this->sizeX = sizeX;
        for (i = 0; i < MAX_SET_TYPES; i++) {
            if (this->dataSet[i]) free(this->dataSet[i]);
            this->dataSet[i] = (double *)calloc(this->sizeX, sizeof(double));
        }
    }

    doComputeFits(pArray);

    int arrayCallbacks = 0;
    getIntegerParam(NDArrayCallbacks, &arrayCallbacks);
    if (arrayCallbacks == 1) {
        NDArray *pArrayOut = this->pNDArrayPool->copy(pArray, NULL, 1);
        if (NULL != pArrayOut) {
            this->getAttributes(pArrayOut->pAttributeList);
            this->unlock();
            doCallbacksGenericPointer(pArrayOut, NDArrayData, 0);
            this->lock();
        }
        else {
            asynPrint(this->pasynUserSelf, ASYN_TRACE_ERROR,
                "%s::%s: Couldn't allocate output array. Further processing terminated.\n",
                driverName, functionName);
        }
    }

    callParamCallbacks();
}

/** Called when asyn clients call pasynFloat64->write().
  * This function performs actions for some parameters.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus  NDPluginFits::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    int peak = 0;
    int idx = 0;
    static const char *functionName = "writeFloat64";

    status = getAddress(pasynUser, &peak);
    if (status != asynSuccess) {
      return status;
    }

    /* Set the parameter and readback in the parameter library.  This may be overwritten when we read back the
     * status at the end, but that's OK */
    status = setDoubleParam(peak, function, value);

    /* Save the user input parameters as initial parameters for each
     * processing. After processing results are set and available to user. */
	idx = peak * 3;
    if (function == NDPluginFitsPeakAmplitude) {
    	idx += 1;
    	this->initialFitParam[idx] = value;
    } else if (function == NDPluginFitsPeakMu) {
    	idx += 2;
    	this->initialFitParam[idx] = value;
    } else if (function == NDPluginFitsPeakSigma) {
    	idx += 3;
    	this->initialFitParam[idx] = value;
    } else if (function == NDPluginFitsBackground) {
    	idx = 0;
    	this->initialFitParam[0] = value;
    } else if (function < FIRST_NDPLUGIN_FITS_PARAM) {
		/* If this parameter belongs to a base class call its method */
    	status = NDPluginDriver::writeFloat64(pasynUser, value);
    }

    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks(peak);
    status = (asynStatus) callParamCallbacks();

    if (status)
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
              "%s:%s: error, status=%d function=%d, value=%f\n",
              driverName, functionName, status, function, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, value=%f\n",
              driverName, functionName, function, value);
    return status;
}


/** Constructor for NDPluginFits; most parameters are simply passed to NDPluginDriver::NDPluginDriver.
  * After calling the base class constructor this method sets reasonable default values for all of the
  * parameters.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] queueSize The number of NDArrays that the input queue for this plugin can hold when
  *            NDPluginDriverBlockingCallbacks=0.  Larger queues can decrease the number of dropped arrays,
  *            at the expense of more NDArray buffers being allocated from the underlying driver's NDArrayPool.
  * \param[in] blockingCallbacks Initial setting for the NDPluginDriverBlockingCallbacks flag.
  *            0=callbacks are queued and executed by the callback thread; 1 callbacks execute in the thread
  *            of the driver doing the callbacks.
  * \param[in] NDArrayPort Name of asyn port driver for initial source of NDArray callbacks.
  * \param[in] NDArrayAddr asyn port driver address for initial source of NDArray callbacks.
  * \param[in] maxPeaks The maximum number of peaks that this plugin will support
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
NDPluginFits::NDPluginFits(const char *portName, int queueSize, int blockingCallbacks,
							const char *NDArrayPort, int NDArrayAddr, int maxPeaks,
							int maxBuffers, size_t maxMemory,
							int priority, int stackSize)
    /* Invoke the base class constructor */
    : NDPluginDriver(portName, queueSize, blockingCallbacks,
                   NDArrayPort, NDArrayAddr, maxPeaks, NUM_NDPLUGIN_FITS_PARAMS, maxBuffers, maxMemory,
                   asynInt32ArrayMask | asynFloat64ArrayMask | asynGenericPointerMask,
                   asynInt32ArrayMask | asynFloat64ArrayMask | asynGenericPointerMask,
				   ASYN_MULTIDEVICE, 1, priority, stackSize)
{
    //static const char *functionName = "NDPluginFits";

    /* Gauss */
    createParam(NDPluginFitsXString,                 asynParamFloat64Array,  &NDPluginFitsX);
    createParam(NDPluginFitsYString,                 asynParamFloat64Array,  &NDPluginFitsY);
    createParam(NDPluginFitsFitString,               asynParamFloat64Array,  &NDPluginFitsFit);
    createParam(NDPluginFitsPeakAmplitudeString,     asynParamFloat64,       &NDPluginFitsPeakAmplitude);
    createParam(NDPluginFitsPeakMuString,            asynParamFloat64,       &NDPluginFitsPeakMu);
    createParam(NDPluginFitsPeakSigmaString,         asynParamFloat64,       &NDPluginFitsPeakSigma);
    createParam(NDPluginFitsBackgroundString,        asynParamFloat64,       &NDPluginFitsBackground);

    /* Make sure that buffers are not allocated */
    for (int i = 0; i < MAX_SET_TYPES; i++) {
    	this->dataSet[i] = NULL;
    }
    /* number of peaks to detect */
	this->maxPeaks = maxPeaks;
	/* for each peak three parameters need to be fitted:
	 * amplitude, mu and sigma */
    this->nrFitParam = this->maxPeaks * 3 + 1;
    this->fitParam = (double *)calloc(this->nrFitParam, sizeof(double));
    this->initialFitParam = (double *)calloc(this->nrFitParam, sizeof(double));

//    setDoubleParam(NDPluginFitsBackground, 0.0);
//    for (int i = 0; i < this->maxPeaks; i++) {
//        setDoubleParam(i, NDPluginFitsPeakAmplitude, 1.0);
//        setDoubleParam(i, NDPluginFitsPeakMu, 1.0);
//        setDoubleParam(i, NDPluginFitsPeakSigma, 1.0);
//        callParamCallbacks(i);
//    }

    /* Set the plugin type string */
    setStringParam(NDPluginDriverPluginType, "NDPluginFits");

    /* Try to connect to the array port */
    connectToArrayPort();
}

/** Configuration command */
extern "C" int NDFitsConfigure(const char *portName, int queueSize, int blockingCallbacks,
								const char *NDArrayPort, int NDArrayAddr,
								int maxPeaks, int maxBuffers, size_t maxMemory,
								int priority, int stackSize)
{
    new NDPluginFits(portName, queueSize, blockingCallbacks, NDArrayPort, NDArrayAddr,
    					maxPeaks, maxBuffers, maxMemory, priority, stackSize);
    return(asynSuccess);
}

/* EPICS iocsh shell commands */
static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg initArg1 = { "frame queue size",iocshArgInt};
static const iocshArg initArg2 = { "blocking callbacks",iocshArgInt};
static const iocshArg initArg3 = { "NDArrayPort",iocshArgString};
static const iocshArg initArg4 = { "NDArrayAddr",iocshArgInt};
static const iocshArg initArg5 = { "maxPeaks",iocshArgInt};
static const iocshArg initArg6 = { "maxBuffers",iocshArgInt};
static const iocshArg initArg7 = { "maxMemory",iocshArgInt};
static const iocshArg initArg8 = { "priority",iocshArgInt};
static const iocshArg initArg9 = { "stackSize",iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1,
                                            &initArg2,
                                            &initArg3,
                                            &initArg4,
                                            &initArg5,
                                            &initArg6,
                                            &initArg7,
                                            &initArg8,
                                            &initArg9};
static const iocshFuncDef initFuncDef = {"NDFitsConfigure",10,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    NDFitsConfigure(args[0].sval, args[1].ival, args[2].ival,
            args[3].sval, args[4].ival, args[5].ival,
            args[6].ival, args[7].ival, args[8].ival, args[9].ival);
}

extern "C" void NDFitsRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

extern "C" {
epicsExportRegistrar(NDFitsRegister);
}
