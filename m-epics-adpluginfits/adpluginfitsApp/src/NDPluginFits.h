#ifndef NDPluginFits_H
#define NDPluginFits_H

#include <epicsTypes.h>

#include "NDPluginDriver.h"

typedef enum {
	dataSetX,
	dataSetY,
	dataSetFit,
} NDFitsSetType;
#define MAX_SET_TYPES dataSetFit+1

#define NDPluginFitsXString                  "FITS_X"                   /* (asynFloat64Array, r/o) x points array */
#define NDPluginFitsYString                  "FITS_Y"                   /* (asynFloat64Array, r/o) y points array */
#define NDPluginFitsFitString                "FITS_FIT"                 /* (asynFloat64Array, r/o) gauss fit array */
#define NDPluginFitsPeakAmplitudeString      "FITS_PEAK_AMPLITUDE"      /* (asynFloat64,      r/o) peak amplitude */
#define NDPluginFitsPeakMuString             "FITS_PEAK_MU"             /* (asynFloat64,      r/o) peak mu */
#define NDPluginFitsPeakSigmaString          "FITS_PEAK_SIGMA"          /* (asynFloat64,      r/o) peak sigma */
#define NDPluginFitsBackgroundString         "FITS_BACKGROUND"          /* (asynFloat64,      r/o) background value */

/** Does curve fitting to Gaussian model.
  */
class epicsShareClass NDPluginFits : public NDPluginDriver {
public:
    NDPluginFits(const char *portName, int queueSize, int blockingCallbacks,
					const char *NDArrayPort, int NDArrayAddr, int maxPeaks,
					int maxBuffers, size_t maxMemory,
					int priority, int stackSize);
    /* These methods override the virtual methods in the base class */
    void processCallbacks(NDArray *pArray);
    asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);

    template <typename epicsType> asynStatus doComputeFitsT(NDArray *pArray);
    asynStatus doComputeFits(NDArray *pArray);

protected:
    int NDPluginFitsX;
#define FIRST_NDPLUGIN_FITS_PARAM NDPluginFitsX
    int NDPluginFitsY;
    int NDPluginFitsFit;
    int NDPluginFitsPeakAmplitude;
    int NDPluginFitsPeakMu;
    int NDPluginFitsPeakSigma;
    int NDPluginFitsBackground;

    int NDPluginFitsDummyEnd;
    #define LAST_NDPLUGIN_FITS_PARAM NDPluginFitsDummyEnd

private:
    epicsInt32 sizeX;
    epicsInt32 maxPeaks;
    double background;
    epicsInt32 nrFitParam;
    double *fitParam;
    double *initialFitParam;
    double *dataSet[MAX_SET_TYPES];
};
#define NUM_NDPLUGIN_FITS_PARAMS ((int)(&LAST_NDPLUGIN_FITS_PARAM - &FIRST_NDPLUGIN_FITS_PARAM + 1))

#endif
