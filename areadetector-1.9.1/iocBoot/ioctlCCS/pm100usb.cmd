# Do not run this file directly!
#
# Include this file in the top level st.cmd if you want support for Thorlabs
# power meter PM100USB.

###############################################################################
# START PM100USB power meter
# usbtmcConfigure(port, vendorNum, productNum, serialNumberStr, priority, flags)
usbtmcConfigure("PM100USB")
asynSetTraceIOMask("PM100USB",0,0xff)
#asynSetTraceMask("PM100USB",0,0xff)

# Load record instances
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/tlPM.template","P=PM100USB:,R=,PORT=PM100USB")
#dbLoadRecords("db/asynRecord.db","P=PM100USB:,R=asyn,PORT=PM100USB,ADDR=0,OMAX=100,IMAX=100")

# END PM100USB power meter
###############################################################################

