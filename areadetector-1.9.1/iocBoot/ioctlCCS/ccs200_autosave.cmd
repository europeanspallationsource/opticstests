epicsEnvSet("PREFIX", "CCS200:")
epicsEnvSet("PORT",   "CCS200")
epicsEnvSet("CAM",    "cam1:")

# save things every thirty seconds
create_monitor_set("auto_settings.req", 30,"P=$(PREFIX),D=$(CAM)")
