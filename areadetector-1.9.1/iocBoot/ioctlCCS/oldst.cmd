< envPaths

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(TOP)/db")

errlogInit(20000)

dbLoadDatabase("$(AREA_DETECTOR)/dbd/tlCCSApp.dbd")
tlCCSApp_registerRecordDeviceDriver(pdbbase) 

###############################################################################
# FOR PM100USB power meter
# usbtmcConfigure(port, vendorNum, productNum, serialNumberStr, priority, flags)
usbtmcConfigure("tlpm1")
asynSetTraceIOMask("tlpm1",0,0xff)
#asynSetTraceMask("tlpm1",0,0xff)

# Load record instances
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/tlPM.template","P=13TLPM1:,R=,PORT=tlpm1")
#dbLoadRecords("db/asynRecord.db","P=tlpm:,R=asyn,PORT=tlpm1,ADDR=0,OMAX=100,IMAX=100")

###############################################################################
# FOR CCSxxx spectrum analyzer

epicsEnvSet("PREFIX", "13TLCCS1:")
epicsEnvSet("PORT",   "TLCCS")
epicsEnvSet("QSIZE",  "20")
#epicsEnvSet("XSIZE",  "2048")
#epicsEnvSet("YSIZE",  "2048")
epicsEnvSet("XSIZE",  "3648")
epicsEnvSet("YSIZE",  "1")
epicsEnvSet("NCHANS", "2048")

# tlCCSConfig(const char *portName, int maxBuffers, size_t maxMemory, 
#                const char *firmwarePath, int priority, int stackSize)
tlCCSConfig("$(PORT)", 0, 0, "/lib/firmware/ccs", 0, 100000)

# Load record instances
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template","P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template","P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/tlCCS.template", "P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1,NELEMENTS=3648")

# Create a standard arrays plugin
NDStdArraysConfigure("Image1", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
# Make NELEMENTS in the following be a little bigger than 2048*2048
# Use the following command for 32-bit images.  This is needed for 32-bit detectors or for 16-bit detectors in acccumulate mode if it would overflow 16 bits
#dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,TYPE=Int32,FTVL=LONG,NELEMENTS=4200000")
# Use the following command for 16-bit images.  This can be used for 16-bit detector as long as accumulate mode would not result in 16-bit overflow
#dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=4200000")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,TYPE=Float64,FTVL=DOUBLE,NELEMENTS=4000")

# Load all other plugins using commonPlugins.cmd
< ../commonPlugins.cmd

#asynSetTraceMask("$(PORT)",0,3)
#asynSetTraceIOMask("$(PORT)",0,4)

iocInit()

# save things every thirty seconds
create_monitor_set("auto_settings.req", 30,"P=$(PREFIX),D=cam1:")
#asynSetTraceMask($(PORT), 0, 255)

epicsThreadSleep 2
#asynReport 2
