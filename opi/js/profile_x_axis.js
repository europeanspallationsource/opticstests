importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

/*
if(triggerPV == pvs[0]) {
	ConsoleUtil.writeInfo("I'm triggered by the first input PV.");
}
if(triggerPV == pvs[1]) {
	ConsoleUtil.writeInfo("I'm triggered by the second input PV.");
}
*/

var extVal = PVUtil.getDouble(pvs[0]);
ConsoleUtil.writeInfo("Ext X axis " + extVal);

if (extVal == 1) {
	// User wants to use external PV for X Axis values..
	widget.setPropertyValue("trace_0_x_pv", "$(P)$(R)ProfileXAxis");
} else {
	// User wants to use internal X Axis values..
	widget.setPropertyValue("trace_0_x_pv", "");
}

var xpv = widget.getPropertyValue("trace_0_x_pv");
ConsoleUtil.writeInfo("Statistics Profile X PV is set to: '" + xpv + "'");

